Namespace.declare ("net.lugdunon.world.defaults.environment.settings");
Namespace.newClass("net.lugdunon.world.defaults.environment.settings.LightingDetailSetting","net.lugdunon.ui.settingsMenu.settings.Setting");

net.lugdunon.world.defaults.environment.settings.LightingDetailSetting.DISPLAY=["Low","Medium","High"];

net.lugdunon.world.defaults.environment.settings.LightingDetailSetting.prototype.init=function(initData)
{
	this.detailButtons=[];
	
	this.callSuper(net.lugdunon.world.defaults.environment.settings.LightingDetailSetting,"init",[{title:"Lighting Detail"}]);
	
	return(this);
};

net.lugdunon.world.defaults.environment.settings.LightingDetailSetting.prototype.saveSetting=function()
{
	this.callSuper(net.lugdunon.world.defaults.environment.settings.LightingDetailSetting,"saveSetting",[]);
	game.environment.setLOD(this.setting.lod+1);
};

net.lugdunon.world.defaults.environment.settings.LightingDetailSetting.prototype.showSetting=function(parent)
{
	var i;
	var button;
	
	this.detailButtons.length=0;
	parent.html(Namespace.requireHTML(this.classId));
	
	for(i=0;i<net.lugdunon.world.defaults.environment.settings.LightingDetailSetting.DISPLAY.length;i++)
	{
		parent.append(
			"<div class='ccButton'><button>"+net.lugdunon.world.defaults.environment.settings.LightingDetailSetting.DISPLAY[i]+"</button></div>"
		);
		button=parent.children(":last").find("button");
		button.on(
			"click",
			{context:this,index:i},
			function(e)
			{
				e.data.context.handleSelection.call(e.data.context,e.data.index);
			}
		);
		
		this.detailButtons.push(button);
		
		if(i == this.setting.lod)
		{
			this.detailButtons[i].addClass("active");
		}
	}
};

net.lugdunon.world.defaults.environment.settings.LightingDetailSetting.prototype.handleSelection=function(index)
{
	if(this.setting.lod != index)
	{
		this.detailButtons[this.setting.lod].removeClass("active");
		this.detailButtons[index           ].addClass   ("active");

		this.setting.lod=index;
	}
};

net.lugdunon.world.defaults.environment.settings.LightingDetailSetting.prototype.getDefaultValue=function()
{
	return({lod:1});
};