Namespace.declare("net.lugdunon.world.defaults.environment.command");

Namespace.newClass("net.lugdunon.world.defaults.environment.command.SetDateCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.world.defaults.environment.command.SetDateCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.command.SetDateCommand,"init",[initData]);
	this.msg;
	
	return(this);
};

net.lugdunon.world.defaults.environment.command.SetDateCommand.prototype.opInit=function(initData)
{
	this.msg=initData;
};

net.lugdunon.world.defaults.environment.command.SetDateCommand.prototype.getCommandLength=function()
{
	if(this.msg != "")
	{
	    return(this.msg.lengthInBytes());
	}
	
	return(0);
};

net.lugdunon.world.defaults.environment.command.SetDateCommand.prototype.buildCommand=function(dataView)
{
	if(this.msg != "")
	{
		dataView.writeString(this.msg);
	}
};

net.lugdunon.world.defaults.environment.command.SetDateCommand.prototype.commandResponse=function(res)
{
	var player =res.readString();
	
	game.environment.setTime(res.readFloat64());

	game.console.log(((player != game.player.name)?(player+" has"):("You have"))+" changed the date to: "+game.environment.getCalendar().toLongDateString()+".","#F00","gmNotification");
	
};