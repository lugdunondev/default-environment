Namespace.declare("net.lugdunon.world.defaults.environment.command");

Namespace.newClass("net.lugdunon.world.defaults.environment.command.SetTimeOfDayCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.world.defaults.environment.command.SetTimeOfDayCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.command.SetTimeOfDayCommand,"init",[initData]);
	this.msg;
	
	return(this);
};

net.lugdunon.world.defaults.environment.command.SetTimeOfDayCommand.prototype.opInit=function(initData)
{
	this.msg=initData;
};

net.lugdunon.world.defaults.environment.command.SetTimeOfDayCommand.prototype.getCommandLength=function()
{
	if(this.msg != "")
	{
	    return(this.msg.lengthInBytes());
	}
	
	return(0);
};

net.lugdunon.world.defaults.environment.command.SetTimeOfDayCommand.prototype.buildCommand=function(dataView)
{
	if(this.msg != "")
	{
		dataView.writeString(this.msg);
	}
};

net.lugdunon.world.defaults.environment.command.SetTimeOfDayCommand.prototype.commandResponse=function(res)
{
	var player =res.readString();
	var hours  =res.readUint32();
	var minutes=res.readUint32();

	game.console.log(((player != game.player.name)?(player+" has"):("You have"))+" changed the time of day to: "+(hours<10?"0":"")+hours+":"+(minutes<10?"0":"")+minutes+".","#F00","gmNotification");
};