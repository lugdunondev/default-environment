package net.lugdunon.world.defaults.environment.command;

import java.io.IOException;
import java.util.List;

import net.lugdunon.command.Command;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.IServerInvokedCommand;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.command.ex.ConsoleFiredCommandNotSupportedException;
import net.lugdunon.command.ex.IsNotServerInvokedCommand;
import net.lugdunon.state.Account;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.character.NonPlayerCharacter;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.state.item.Item;
import net.lugdunon.state.item.ItemInstance;
import net.lugdunon.state.item.action.BaseChargedAction;
import net.lugdunon.state.item.presence.ActionPresence;
import net.lugdunon.util.FastMath;
import net.lugdunon.world.biome.Biome;
import net.lugdunon.world.defaults.environment.weather.CommandableWeather;
import net.lugdunon.world.environment.hz.HardinessZone;
import net.lugdunon.world.environment.weather.BaseWeather;
import net.lugdunon.world.instance.Instance;

import org.json.JSONObject;

public class ManageWeatherCommand extends Command implements IServerInvokedCommand
{
	protected Response iRes;

	////
	
	@Override
	public String getCommandId()
	{
	    return("CORE.COMMAND.ENVIRONMENT.MANAGE.WEATHER");
	}

	@Override
	public String getName()
	{
		return("Manage Weather");
	}

	@Override
	public String getDescription()
	{
		return("Manages weather (start / finish).");
	}

	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

	@Override
	public void handle(CommandRequest request) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		try
		{
			AlterWeatherChargedAction awca;
			CommandProperties         caProps=new CommandProperties();
			Character                 c      =request.getOrigin().getAccount().getActiveCharacter();
			int                       index  =request.getData().read();
			int                       block  =Character.ACTION_PLAY_BLOCK;
			ItemInstance              ii     =c.getInventoryItem(index,block);
			Item                      i      =ii.getItemDef();
			
		    if(!canPerformAction(c))
		    {
		    	return;
		    }
			
			if(!c.knowsSpell(i.getItemId()))
			{
				return;
			}

			if(c.getCharacterStats().isRecharging(i))
			{
				return;
			}
			
			caProps.setCharacter("actor",c);
			
			awca=new AlterWeatherChargedAction(caProps,i);
			
			if(i.getToolCharge() == 0)
			{
				awca.actionFullyCharged();
			}
			else
			{
				c.getCharacterStats().itemChargeInitiated(awca);
			}
			
			Response oRes;
			
			iRes=initializeInternalResponse();
			
		    iRes.out.writeBoolean(false);
		    
		    oRes=initializeResponse();
		    oRes.out.write(iRes.bytes());
		    
		    request.getOrigin().sendMessage(oRes.bytes());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }

	@Override
    public void handle(CommandProperties props) throws IOException, ConsoleFiredCommandNotSupportedException
    {
		int mode=props.getInt("mode");
		
		iRes=initializeInternalResponse();
		
		iRes.out.writeBoolean(true);
		iRes.out.write       (mode);
		
		if(mode == CommandableWeather.COMMAND_MODE_PLAYER_ALTER)
		{
			handlePlayerAlter(props);
		}
		
		if(mode == CommandableWeather.COMMAND_MODE_TOTAL_UPDATE)
		{
			handleTotalUpdate(props);
		}
		else if(
			(mode == CommandableWeather.COMMAND_MODE_WEATHER_START ) ||
			(mode == CommandableWeather.COMMAND_MODE_WEATHER_FINISH)
		)
		{
			handleLifecycle(props, mode);
		}
    }
	
	////

	private void handlePlayerAlter(CommandProperties props) throws IOException
    {
	    Response oRes;
	    Character       c       =props.getCharacter("character");
	    Item            spell   =props.getItem     ("spell"    );
	    Instance        instance=State.instance().getWorld().getInstance(c.getInstanceId());
	    Biome           biome   =null;
	    
	    biome=instance.getTerrain().getBiome(
    		instance.getTerrain().getBiomeAt(
    			c.getLocation().getX(),
    			c.getLocation().getY()
    		)
    	);
	    
	    if(biome != null)
	    {
	    	try
	    	{
		    	HardinessZone hz=State.instance().getWorld().getSubsystems().getEnvironment().getHardinessZone(biome.getHardinessZone());
		    	BaseWeather   w =null;
		    	long          d =FastMath.valueFromRandomDefinition(spell.getToolDef().getJSONObject("value"));
		    	
		    	if(spell.getToolDef().has("weatherType") && !spell.getToolDef().isNull("weatherType"))
		    	{
		    		w=(BaseWeather) ManageWeatherCommand.class.getClassLoader().loadClass(
		    			spell.getToolDef().getJSONObject("weatherType").getString("class")
		    		).getConstructor(
		    			new Class[]
		    			{
		    				String.class,
		    				HardinessZone.class,
		    				JSONObject.class
		    			}
		    		).newInstance(
		    			"PA_"+((c instanceof NonPlayerCharacter)?((NonPlayerCharacter) c).getNpcId():c.getName())+"_"+hz.getId(),
		    			hz,
		    			spell.getToolDef().getJSONObject("weatherType").getJSONObject("args")
		    		);
		    	}
		    	
		    	hz.setCurrentWeather(w,d);
	
		    	iRes.out.writeBoolean(hz.getCurrentWeather() != null);
		    	
		    	if(hz.getCurrentWeather() != null)
		    	{
			    	iRes.out.writeUTF       (hz.getCurrentWeather().getId                 ());
			    	iRes.out.writeUTF       (hz.getCurrentWeather().getClientSideImplClass());
			    	iRes.out.writeJSONObject(hz.getCurrentWeather().getClientSideImplArgs ());
		    	}
		    	
		    	oRes=initializeResponse();
		    	oRes.out.write(iRes.bytes());
	
		    	for(PlayerCharacter pc:State.instance().listActiveCharactersInInstance(instance.getInstanceId()))
		    	{
		    		Biome pBiome=null;
		    		
		    		if(pc.getAccount().getGameMode() == Account.MODE_PLAY)
		    		{
		    			pBiome=instance.getTerrain().getBiome(
		    				instance.getTerrain().getBiomeAt(
		    					pc.getLocation().getX(),
		    					pc.getLocation().getY()
		    				)
		    			);
		    		}
		    		else if(pc.getAccount().getGameMode() == Account.MODE_EDIT)
		    		{
		    			pBiome=instance.getTerrain().getBiome(
		    				instance.getTerrain().getBiomeAt(
		    					pc.getAccount().getEditingLocation().getX(),
		    					pc.getAccount().getEditingLocation().getY()
		    				)
		    			);
		    		}
		    		
		    		// CHARACTER IS IN BIOME WITH WEATHER'S ASSOCIATED HARDINESS ZONE
		    		if((pBiome != null) && (pBiome.getHardinessZone() == hz.getId()))
		    		{
		    			pc.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
		    		}
			    }
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    }
    }

	private void handleTotalUpdate(CommandProperties props) throws IOException
    {
	    Response oRes;
	    PlayerCharacter pc=props.getPlayerCharacter("character");
	    Instance        instance=State.instance().getWorld().getInstance(pc.getInstanceId());
	    Biome           biome   =null;
	    
	    if(pc.getAccount().getGameMode() == Account.MODE_PLAY)
	    {
	    	biome=instance.getTerrain().getBiome(
	    		instance.getTerrain().getBiomeAt(
	    			pc.getLocation().getX(),
	    			pc.getLocation().getY()
	    		)
	    	);
	    }
	    else if(pc.getAccount().getGameMode() == Account.MODE_EDIT)
	    {
	    	biome=instance.getTerrain().getBiome(
	    		instance.getTerrain().getBiomeAt(
	    			pc.getAccount().getEditingLocation().getX(),
	    			pc.getAccount().getEditingLocation().getY()
	    		)
	    	);
	    }
	    
	    {
		    if(biome != null)
		    {
		    	HardinessZone hz=State.instance().getWorld().getSubsystems().getEnvironment().getHardinessZone(biome.getHardinessZone());
	
		    	iRes.out.writeBoolean(hz.getCurrentWeather() != null);
		    	
		    	if(hz.getCurrentWeather() != null)
		    	{
			    	iRes.out.writeUTF       (hz.getCurrentWeather().getId                 ());
			    	iRes.out.writeUTF       (hz.getCurrentWeather().getClientSideImplClass());
			    	iRes.out.writeJSONObject(hz.getCurrentWeather().getClientSideImplArgs ());
		    	}
		    }
		    else
		    {
		    	iRes.out.writeBoolean(false);
		    }
	    	
	    	oRes=initializeResponse();
	    	oRes.out.write(iRes.bytes());
	    	
	    	pc.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
	    }
    }

	private void handleLifecycle(CommandProperties props, int mode) throws IOException
    {
	    Response       oRes;
		List<Instance> activeInstances=State.instance().getWorld().listActiveInstances();
	    HardinessZone  hardinessZone  =State.instance().getWorld().getSubsystems().getEnvironment().getHardinessZone(props.getInt("hardinessZone"));
	    
	    iRes.out.writeUTF(props.getString ("weatherId"));
	    
	    if(mode == CommandableWeather.COMMAND_MODE_WEATHER_START)
	    {
	    	iRes.out.writeUTF       (props.getString    ("implClass"));
	    	iRes.out.writeJSONObject(props.getJSONObject("implArgs" ));
	    }
	    
	    oRes=initializeResponse();
	    oRes.out.write(iRes.bytes());
	    
	    for(Instance instance:activeInstances)
	    {
	    	for(PlayerCharacter pc:State.instance().listActiveCharactersInInstance(instance.getInstanceId()))
	    	{
	    		Biome biome=null;
	    		
	    		if(pc.getAccount().getGameMode() == Account.MODE_PLAY)
	    		{
	    			biome=instance.getTerrain().getBiome(
	    				instance.getTerrain().getBiomeAt(
	    					pc.getLocation().getX(),
	    					pc.getLocation().getY()
	    				)
	    			);
	    		}
	    		else if(pc.getAccount().getGameMode() == Account.MODE_EDIT)
	    		{
	    			biome=instance.getTerrain().getBiome(
	    				instance.getTerrain().getBiomeAt(
	    					pc.getAccount().getEditingLocation().getX(),
	    					pc.getAccount().getEditingLocation().getY()
	    				)
	    			);
	    		}
	    		
	    		// CHARACTER IS IN BIOME WITH WEATHER'S ASSOCIATED HARDINESS ZONE
	    		if((biome != null) && (biome.getHardinessZone() == hardinessZone.getId()))
	    		{
	    			pc.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
	    		}
	    	}
	    }
    }
	
	////
	

	
	private static class AlterWeatherChargedAction extends BaseChargedAction
	{
		private CommandProperties caProps;
		
		public AlterWeatherChargedAction(CommandProperties caProps, Item item)
        {
	        super(item,ActionPresence.ACTION_PRESENCE_TYPE_TOOL);
	        this.caProps=caProps;
        }

		@Override
        public void actionInterrupted()
        {
	        ;
        }

		@Override
        public void actionFullyCharged()
		{
			Character c=caProps.getCharacter("actor" );	
				
			//CHECK THAT ITEM IS A DIRECT HEAL SPELL, AND SPELL ISN'T ON COOLDOWN
			if(
				(getItem() != null) && 
				("SPELL".equals(getItem().getItemType()) && "ALTER.WEATHER".equals(getItem().getItemSpellType())) &&
				!c.getCharacterStats().isRecharging(getItem()) &&
				c.getCharacterStats().itemUsedAsTool(getItem())
			)
			{
				//STAMINA CHECK
				if(!c.getCharacterStats().satisfiesStaminaRequirement(
						getItem(),
						Item.ITEM_USE_TOOL,
						true
					)
				)
				{
					return;
				}
				
				try
	            {
					CommandProperties apProps=new CommandProperties();
					
					apProps.setCharacter("character",      c);
					apProps.setItem     ("item",           getItem());
					apProps.setString   ("actionType",     ActionPresence.ACTION_PRESENCE_TYPE_TOOL);
					apProps.setBoolean  ("triggerRecharge",getItem().getToolRecharge() > 0);
					
				    State.instance().getGame().addIncomingRequest(
						"CORE.COMMAND.ACTION.PERFORMED",
						apProps
				    );
	            }
	            catch (CommandNotSupportedException e)
	            {
		            e.printStackTrace();
	            }
	            catch (IsNotServerInvokedCommand e)
	            {
		            e.printStackTrace();
	            }
				
				try
	            {
					CommandProperties props=new CommandProperties();
					
					props.setInt      ("mode",     CommandableWeather.COMMAND_MODE_PLAYER_ALTER);
					props.setCharacter("character",c                                           );
					props.setItem     ("spell",    getItem()                                   );
					
		            State.instance().getGame().addIncomingRequest(
		            	"CORE.COMMAND.ENVIRONMENT.MANAGE.WEATHER",
		            	props
		            );
	            }
	            catch (CommandNotSupportedException e)
	            {
		            e.printStackTrace();
	            }
	            catch (IsNotServerInvokedCommand e)
	            {
		            e.printStackTrace();
	            }
			}
        }
	}
}