Namespace.declare("net.lugdunon.command.core.player");

Namespace.newClass("net.lugdunon.world.defaults.environment.command.ManageWeatherCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.COMMAND_MODE_TOTAL_UPDATE  =0;
net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.COMMAND_MODE_WEATHER_START =1;
net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.COMMAND_MODE_WEATHER_FINISH=2;
net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.COMMAND_MODE_PLAYER_ALTER  =3;

net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.command.ManageWeatherCommand,"init",[initData]);

	this.actionBarIndex;
	
	return(this);
};

net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.prototype.opInit=function(initData)
{
	this.actionBarIndex=initData.actionBarIndex;
};

net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.prototype.getCommandLength=function()
{
    return(1);
};

net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.prototype.buildCommand=function(dataView)
{
	dataView.writeUint8(this.actionBarIndex);
};

net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.prototype.commandResponse=function(res)
{
	if(res.readBoolean())
	{
		var mode=res.readUint8();

		if(mode == net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.COMMAND_MODE_PLAYER_ALTER)
		{
			var has=res.readBoolean();
			var wid=null;
			var cls=null;
			var arg=null;
			var int=null;
			
			if(has)
			{
				wid=res.readString();
				cls=res.readString();
				arg=res.readObject();
			}
			
			int=setInterval(
				function()
				{
					if(game.environment)
					{
						game.environment.clearWeather();
						
						if(has)
						{
							has=false;

							game.environment.makeWeather (
								wid,
								cls,
								arg
							);
						}
						
						clearInterval(int);
					}
				},
				100
			);
		}
		if(mode == net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.COMMAND_MODE_TOTAL_UPDATE)
		{
			var has=res.readBoolean();
			var wid=null;
			var cls=null;
			var arg=null;
			var int=null;
			
			if(has)
			{
				wid=res.readString();
				cls=res.readString();
				arg=res.readObject();
			}
			
			int=setInterval(
				function()
				{
					if(game.environment)
					{
						game.environment.clearWeather();
						
						if(has)
						{
							has=false;

							game.environment.makeWeather (
								wid,
								cls,
								arg
							);
						}
						
						clearInterval(int);
					}
				},
				100
			);
		}
		else if(mode == net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.COMMAND_MODE_WEATHER_START )
		{
			var wid=res.readString();
			var cls=res.readString();
			var arg=res.readObject();
			var int=null;
			
			int=setInterval(
				function()
				{
					if(game.environment)
					{
						game.environment.clearWeather();
						game.environment.makeWeather (
							wid,
							cls,
							arg
						);
						
						clearInterval(int);
					}
				},
				100
			);
		}
		else if(mode == net.lugdunon.world.defaults.environment.command.ManageWeatherCommand.COMMAND_MODE_WEATHER_FINISH)
		{
			var wid=res.readString();
			var int=null;
			
			int=setInterval(
				function()
				{
					if(game.environment)
					{
						game.environment.clearWeather(
							wid
						);
						
						clearInterval(int);
					}
				},
				100
			);
		}
	}
};