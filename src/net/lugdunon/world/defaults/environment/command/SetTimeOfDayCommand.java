package net.lugdunon.world.defaults.environment.command;

import java.io.IOException;

import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.console.gm.GmOnlyConsoleFiredCommand;
import net.lugdunon.state.State;
import net.lugdunon.world.environment.calendar.BaseCalendar;

public class SetTimeOfDayCommand extends GmOnlyConsoleFiredCommand
{
	Response iRes;

	@Override
    public String getConsoleOpCodeAlias()
    {
	    return("time");
    }

	@Override
    public String[] getConsoleOpCodeAliases()
    {
	    return(null);
    }

	@Override
    public ConsoleCommandParameter[] getConsoleCommandParameters()
    {
	    return(
	    	new ConsoleCommandParameter[]
	    	{
	    		new ConsoleCommandParameter("{hours:minutes:seconds}","Sets the time to the specified hours, minutes, and seconds.")
    		}
	    );
    }
	
	@Override
	public String getCommandId()
	{
	    return("CORE.COMMAND.ENVIRONMENT.SET.TOD");
	}

	@Override
	public String getName()
	{
	    return("Set Time of Day");
	}
	
	@Override
	public String getDescription()
	{
	    return("Sets the time of day.");
	}
	
	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

	@Override
    public void handleAsGm(CommandRequest request, String consoleMessage) throws IOException
    {
		if(consoleMessage.equals(""))
		{
			handleAsPlayer(request, consoleMessage);
			return;
		}
		
		if(State.instance().isGm(request.getOrigin().getAccount().getAccountName()))
		{
			try
			{
				String[] hm=consoleMessage.split(":");
				
				if(hm.length == 2)
				{
					BaseCalendar c=State.instance().getWorld().getSubsystems().getEnvironment().getCalendar();
					int          h=Math.abs(Integer.parseInt(hm[0]));
					int          m=Math.abs(Integer.parseInt(hm[1]));
					
					if(h >= 0 && h < c.getMaximumAllowableHour())
					{
						if(m >= 0 && m < c.getMaximumAllowableMinute())
						{
							c.setHours  (h);
							c.setMinutes(m);
							
							iRes=initializeInternalResponse();
			
							iRes.out.writeUTF(request.getOrigin().getAccount().getActiveCharacter().getName());
							
							iRes.out.writeInt(c.getHours  ());
							iRes.out.writeInt(c.getMinutes());
			
							Response oRes=initializeResponse();
							
							oRes.out.write(iRes.bytes());
							
							request.getOrigin().sendMessage(oRes.bytes());
						}
					}
				}
			}
			catch(Exception e)
			{
				;
			}
		}
    }
	
	public boolean hasClientSide()
	{
		return(true);
	}
}
