package net.lugdunon.world.defaults.environment.command;

import java.io.IOException;

import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.console.gm.GmOnlyConsoleFiredCommand;
import net.lugdunon.state.State;
import net.lugdunon.world.environment.calendar.BaseCalendar;

public class SetDateCommand extends GmOnlyConsoleFiredCommand
{
	Response iRes;
	
	@Override
    public String getConsoleOpCodeAlias()
    {
	    return("date");
    }

	@Override
    public String[] getConsoleOpCodeAliases()
    {
	    return(null);
    }

	@Override
    public ConsoleCommandParameter[] getConsoleCommandParameters()
    {
	    return(
	    	new ConsoleCommandParameter[]
	    	{
	    		new ConsoleCommandParameter("{day/month/year}","Sets the date to the specified day, month, and year.")
    		}
	    );
    }
	
	@Override
	public String getCommandId()
	{
	    return("CORE.COMMAND.ENVIRONMENT.SET.DATE");
	}

	@Override
	public String getName()
	{
	    return("Set Date");
	}
	
	@Override
	public String getDescription()
	{
	    return("Sets the date.");
	}
	
	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

	@Override
    public void handleAsGm(CommandRequest request, String consoleMessage) throws IOException
    {
		if(consoleMessage.equals(""))
		{
			handleAsPlayer(request, consoleMessage);
			return;
		}
		
		if(State.instance().isGm(request.getOrigin().getAccount().getAccountName()))
		{
			try
			{
				String[] dmy=consoleMessage.split("/");
				
				if(dmy.length == 3)
				{
					BaseCalendar c=State.instance().getWorld().getSubsystems().getEnvironment().getCalendar();
					int          d=Math.abs(Integer.parseInt(dmy[0]));
					int          m=Math.abs(Integer.parseInt(dmy[1]));
					int          y=Math.abs(Integer.parseInt(dmy[2]));
					
					if(d > 0 && d < c.getMaximumAllowableDate())
					{
						if(m > 0 && m < (c.getMaximumAllowableMonth()+1))
						{
							c.setDate (d);
							c.setMonth(m);
							c.setYear (y);

							iRes=initializeInternalResponse();
			
							iRes.out.writeUTF   (request.getOrigin().getAccount().getActiveCharacter().getName());
							iRes.out.writeDouble(c.getTime()                                                    );
			
							Response oRes=initializeResponse();
							
							oRes.out.write(iRes.bytes());
							
							request.getOrigin().sendMessage(oRes.bytes());
						}
					}
				}
			}
			catch(Exception e)
			{
				;
			}
		}
    }
	
	public boolean hasClientSide()
	{
		return(true);
	}
}
