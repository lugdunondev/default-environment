package net.lugdunon.world.defaults.environment.calendar;

import net.lugdunon.world.environment.calendar.BaseCalendar;


public class SimpleGaulishCalendar extends BaseCalendar
{
//	private final double SUNRISE_SUNSET_VARIANCE=0.25;
	
	private static final String[] LUNAR_PHASE_NAMES={
		"New Moon",
		"Waxing Crescent",
		"First Quarter",
		"Waxing Gibbous",
		"Full Moon",
		"Waning Gibbous", 
		"Third Quarter",
		"Waning Crescent"
	};

	private static final String[] SEASON_NAMES={
		"Samon",     //SUMMER
		"Anagantion",//FALL
		"Giamon",    //WINTER
		"Elembivon"  //SPRING
	};
	
	private static final String[] MONTH_NAMES={
		"Samonios",   //JUN
		"Dumanios",   //JUL
		"Riuros",     //AUG
		"Anagantios", //SEP
		"Ogronnios",  //OCT
		"Cutios",     //NOV
		"Giamonios",  //DEC
		"Simivisonna",//JAN
		"Equos",      //FEB
		"Elembivos",  //MAR
		"Aedrinios",  //APR
		"Cantlos"     //MAY
	};
	
	private static final String[] MONTH_NAMES_ABBREV={
		"Sam.",
		"Dum.",
		"Riu.",
		"Ana.",
		"Ogr.",
		"Cut.",
		"Gia.",
		"Sim.",
		"Equ.",
		"Ele.",
		"Aed.",
		"Can."
	};
	
	public static final int MONTH_SAMONIOS   = 0;
	public static final int MONTH_DUMANIOS   = 1;
	public static final int MONTH_RIUROS     = 2;
	public static final int MONTH_ANAGANTIOS = 3;
	public static final int MONTH_OGRONNIOS  = 4;
	public static final int MONTH_CUTIOS     = 5;
	public static final int MONTH_GIAMONIOS  = 6;
	public static final int MONTH_SIMIVISONNA= 7;
	public static final int MONTH_EQUOS      = 8;
	public static final int MONTH_ELEMBIVOS  = 9;
	public static final int MONTH_AEDRINIOS  =10;
	public static final int MONTH_CANTLOS    =11;
	
	private static final long SECONDS_IN_MINUTE=60;
	private static final long MINUTES_IN_HOUR  =60;
	private static final long HOURS_IN_DAY     =24;
	private static final long DAYS_IN_WEEK     = 7;
	private static final long WEEKS_IN_MONTH   = 4;
	private static final long MONTHS_IN_SEASON = 3;
	private static final long MONTHS_IN_YEAR   =12;

	private static final long SECONDS_IN_HOUR  =SECONDS_IN_MINUTE*MINUTES_IN_HOUR;
	private static final long SECONDS_IN_DAY   =SECONDS_IN_HOUR  *HOURS_IN_DAY;
	private static final long SECONDS_IN_WEEK  =SECONDS_IN_DAY   *DAYS_IN_WEEK;
	private static final long SECONDS_IN_MONTH =SECONDS_IN_WEEK  *WEEKS_IN_MONTH;
	private static final long SECONDS_IN_SEASON=SECONDS_IN_MONTH *MONTHS_IN_SEASON;
	private static final long SECONDS_IN_YEAR  =SECONDS_IN_MONTH *MONTHS_IN_YEAR;
	
	private static final int  SECONDS          = 0;
	private static final int  MINUTES          = 1;
	private static final int  HOURS            = 2;
	private static final int  DAYS             = 3;
	private static final int  WEEKS            = 4;
	private static final int  MONTHS           = 5;
	private static final int  YEARS            = 6;
	
	private static final int  SUMMER_SOLSTICE  = 1;
	private static final int  FALL_EQUINOX     = 4;
	private static final int  WINTER_SOLSTICE  = 7;
	private static final int  SPRING_EQUINOX   =10;
	
	private int[] parts;
	
//	public static void main(String[] args)
//	{
//		SimpleGaulishCalendar cal=new SimpleGaulishCalendar(30274888);
//
//		String[] dmy="8/1/1".split("/");
//
//		cal.setDate (Math.abs(Integer.parseInt(dmy[0])));
//		cal.setMonth(Math.abs(Integer.parseInt(dmy[1])));
//		cal.setYear (Math.abs(Integer.parseInt(dmy[2])));
//
//		System.out.println("D: "+cal.getDay  ());
//		System.out.println("W: "+cal.getWeek ());
//		System.out.println("m: "+cal.getMonth());
//		System.out.println("Y: "+cal.getYear ());
//		System.out.println(      cal.getTime ());
//		System.out.println();
//	}
	
	public SimpleGaulishCalendar()
	{
		super();
	}
	
	public SimpleGaulishCalendar(long time)
	{
		super(time);
	}
	
	private void parse()
	{
		long time=getTime();
		
		parts=new int[7];
		
		if(time/SECONDS_IN_YEAR > 0)
		{
			parts[YEARS] =(int) (time/SECONDS_IN_YEAR);
			time        -=parts[YEARS]*SECONDS_IN_YEAR;
		}
		else
		{
			parts[YEARS]=0;
		}

		if(time/SECONDS_IN_MONTH > 0)
		{
			parts[MONTHS] =(int) (time/SECONDS_IN_MONTH);
			time         -=parts[MONTHS]*SECONDS_IN_MONTH;
		}
		else
		{
			parts[MONTHS]=0;
		}

		if(time/SECONDS_IN_WEEK > 0)
		{
			parts[WEEKS] =(int) (time/SECONDS_IN_WEEK);
			time         -=parts[WEEKS]*SECONDS_IN_WEEK;
		}
		else
		{
			parts[WEEKS]=0;
		}

		if(time/SECONDS_IN_DAY > 0)
		{
			parts[DAYS] =(int) (time/SECONDS_IN_DAY);
			time         -=parts[DAYS]*SECONDS_IN_DAY;
		}
		else
		{
			parts[DAYS]=0;
		}

		if(time/SECONDS_IN_HOUR > 0)
		{
			parts[HOURS] =(int) (time/SECONDS_IN_HOUR);
			time         -=parts[HOURS]*SECONDS_IN_HOUR;
		}
		else
		{
			parts[HOURS]=0;
		}

		if(time/SECONDS_IN_MINUTE > 0)
		{
			parts[MINUTES] =(int) (time/SECONDS_IN_MINUTE);
			time         -=parts[MINUTES]*SECONDS_IN_MINUTE;
		}
		else
		{
			parts[MINUTES]=0;
		}
		
		parts[SECONDS]=(int) time;
	}
	
	@Override
	public void setTime(long time)
	{
		super.setTime(time);
		
		parse();
	}
	
	@Override
    public int getSeconds()
    {
	    return(parts[SECONDS]);
    }

	@Override
    public void setSeconds(int seconds)
    {
	    setTime((getTime()-getSeconds())+seconds);
    }

	@Override
    public int getMinutes()
    {
	    return(parts[MINUTES]);
    }

	@Override
    public void setMinutes(int minutes)
    {
	    setTime((getTime()-(getMinutes()*SECONDS_IN_MINUTE))+(minutes*SECONDS_IN_MINUTE));
    }

	@Override
    public int getHours()
    {
	    return(parts[HOURS]);
    }

	@Override
    public void setHours(int hours)
    {
	    setTime((getTime()-(getHours()*SECONDS_IN_HOUR))+(hours*SECONDS_IN_HOUR));
    }

	@Override
    public int getDay()
    {
	    return(parts[DAYS]);
    }

	@Override
    public void setDay(int day)
    {
	    setTime((getTime()-(getDay()*SECONDS_IN_DAY))+((day-1)*SECONDS_IN_DAY));
    }
	
	@Override
    public int getDate()
    {
	    return((int) (parts[WEEKS]*DAYS_IN_WEEK)+parts[DAYS]);
    }
	
	@Override
    public void setDate(int day)
    {
		if(day/DAYS_IN_WEEK > 0)
		{
			setWeek((int) (day/DAYS_IN_WEEK)+1);
			day -=(int) (day/DAYS_IN_WEEK)*DAYS_IN_WEEK;
		}
		else
		{
			setWeek(1);
		}
		
		setDay(day);
    }

	@Override
    public int getWeek()
    {
	    return(parts[WEEKS]);
    }

	@Override
    public void setWeek(int week)
    {
	    setTime((getTime()-(getWeek()*SECONDS_IN_WEEK))+((week-1)*SECONDS_IN_WEEK));
    }

	@Override
	public String getMonthName()
	{
		return(MONTH_NAMES[getMonth()]);
	}

	@Override
	public String getShortMonthName()
	{
		return(MONTH_NAMES_ABBREV[getMonth()]);
	}

	@Override
    public int getMonth()
    {
	    return(parts[MONTHS]);
    }

	@Override
    public void setMonth(int month)
    {
	    setTime((getTime()-(getMonth()*SECONDS_IN_MONTH))+((month-1)*SECONDS_IN_MONTH));
    }

	@Override
    public int getYear()
    {
	    return(parts[YEARS]);
    }

	@Override
    public void setYear(int year)
    {
	    setTime((getTime()-(getYear()*SECONDS_IN_YEAR))+(year*SECONDS_IN_YEAR));
    }

	@Override
    public double getLunarPhase()
    {
	    return(((double) getDay()+getWeek()*DAYS_IN_WEEK)/(DAYS_IN_WEEK*WEEKS_IN_MONTH));
    }

	@Override
    public String getLunarPhaseName()
    {
		if(getDate() == 0)
		{
			return(LUNAR_PHASE_NAMES[0]);
		}
		else if(getDate() > 0 && getDate() < 7)
		{
			return(LUNAR_PHASE_NAMES[1]);
		}
		else if(getDate() == 7)
		{
			return(LUNAR_PHASE_NAMES[2]);
		}
		else if(getDate() > 7 && getDate() < 14)
		{
			return(LUNAR_PHASE_NAMES[3]);
		}
		else if(getDate() == 14)
		{
			return(LUNAR_PHASE_NAMES[4]);
		}
		else if(getDate() > 14 && getDate() < 21)
		{
			return(LUNAR_PHASE_NAMES[5]);
		}
		else if(getDate() == 21)
		{
			return(LUNAR_PHASE_NAMES[6]);
		}
		else
		{
			return(LUNAR_PHASE_NAMES[7]);
		}
    }

	@Override
    public long getSunrise()
    {
		//6 AM
	    return(SECONDS_IN_HOUR* 6);
    }

	@Override
    public long getSunset()
    {
		//6 PM
	    return(SECONDS_IN_HOUR*18);
    }

	@Override
    public double getSolarPosition()
    {
		double tod=getTimeOfDayAsPercent();
		
		if(tod < 0.5)
		{
			return(tod*2.0);
		}
		else
		{
			return(1.0-((tod-0.5)*2.0));
		}
    }

	@Override
    public boolean isTodaySummerSolstice()
    {
	    return(getMonth() == SUMMER_SOLSTICE && getWeek() == 2);
    }

	@Override
    public boolean isTodayWinterSolstice()
    {
	    return(getMonth() == WINTER_SOLSTICE && getWeek() == 2);
    }

	@Override
    public boolean isTodaySpringEquinox()
    {
	    return(getMonth() == SPRING_EQUINOX && getWeek() == 2);
    }

	@Override
    public boolean isTodayFallEquinox()
    {
	    return(getMonth() == FALL_EQUINOX && getWeek() == 2);
    }

	@Override
    public boolean isDaytime()
    {
	    return(getTimeOfDay() >= getSunrise() && getTimeOfDay() < getSunset());
    }

	@Override
    public long getTimeOfDay()
    {
	    return((getHours()*SECONDS_IN_HOUR)+(getMinutes()*SECONDS_IN_MINUTE)+getSeconds());
    }

	@Override
    public double getTimeOfDayAsPercent()
    {
	    return(((double) getTimeOfDay())/SECONDS_IN_DAY);
    }
	
	////

	@Override
	public int getSecondsInMinute()
    {
	    return((int) SECONDS_IN_MINUTE);
    }
	
	@Override
	public int getMinutesInHour  ()
    {
	    return((int) MINUTES_IN_HOUR);
    }
	
	@Override
	public int getHoursInDay     ()
    {
	    return((int) HOURS_IN_DAY);
    }
	
	@Override
	public int getDaysInWeek     ()
    {
	    return((int) DAYS_IN_WEEK);
    }
	
	@Override
	public int getWeeksInMonth   ()
    {
	    return((int) WEEKS_IN_MONTH);
    }
	
	@Override
	public int getMonthsInSeason ()
    {
	    return((int) MONTHS_IN_SEASON);
    }
	
	@Override
	public int getSeasonsInYear  ()
    {
	    return((int) (MONTHS_IN_YEAR/MONTHS_IN_SEASON));
    }
	
	////

	@Override
	public int getMaximumAllowableSecond()
    {
	    return((int) SECONDS_IN_DAY);
    }

	@Override
	public int getMaximumAllowableMinute()
    {
	    return((int) MINUTES_IN_HOUR);
    }

	@Override
	public int getMaximumAllowableHour()
    {
	    return((int) HOURS_IN_DAY);
    }

	@Override
	public int getMaximumAllowableDay()
    {
	    return((int) DAYS_IN_WEEK);
    }

	@Override
	public int getMaximumAllowableDate()
    {
	    return((int) (WEEKS_IN_MONTH*DAYS_IN_WEEK));
    }

	@Override
	public int getMaximumAllowableWeek()
    {
	    return((int) WEEKS_IN_MONTH);
    }

	@Override
	public int getMaximumAllowableMonth()
    {
	    return((int) MONTHS_IN_YEAR);
    }

	@Override
	public int getMaximumAllowableSeason()
    {
	    return(SEASON_NAMES.length-1);
    }

	@Override
    public String getSeasonDetailName()
    {
		String prefix="Late ";
		
		if(getSeasonProgress() < 0.34)
		{
			prefix="Early ";
		}
		else if(getSeasonProgress() < 0.67)
		{
			prefix="Mid-";
		}
		
	    return(prefix+getSeasonName());
    }

	@Override
    public String getSeasonName()
    {
	    return(SEASON_NAMES[getSeason()]);
    }

	@Override
    public int getSeason()
    {
	    return((int) (getMonth() / (MONTHS_IN_YEAR/(MONTHS_IN_YEAR/MONTHS_IN_SEASON))));
    }

	@Override
    public double getSeasonProgress()
    {
	    return(((double) (getTime() % SECONDS_IN_SEASON))/SECONDS_IN_SEASON);
    }
}