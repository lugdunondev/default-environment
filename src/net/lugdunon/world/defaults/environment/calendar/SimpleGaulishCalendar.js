Namespace.declare ("net.lugdunon.world.defaults.environment.calendar");
Namespace.newClass("net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar","net.lugdunon.world.environment.calendar.BaseCalendar");

//net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SUNRISE_SUNSET_VARIANCE=0.25;

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES=[
	"New Moon",
	"Waxing Crescent",
	"First Quarter",
	"Waxing Gibbous",
	"Full Moon",
	"Waning Gibbous", 
	"Third Quarter",
	"Waning Crescent"
];

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SEASON_NAMES=[
	"Samon",     //SUMMER
	"Anagantion",//FALL
	"Giamon",    //WINTER
	"Elembivon"  //SPRING
];

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_NAMES=[
	"Samonios",   //JUN
	"Dumanios",   //JUL
	"Riuros",     //AUG
	"Anagantios", //SEP
	"Ogronnios",  //OCT
	"Cutios",     //NOV
	"Giamonios",  //DEC
	"Simivisonna",//JAN
	"Equos",      //FEB
	"Elembivos",  //MAR
	"Aedrinios",  //APR
	"Cantlos"     //MAY
];

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_NAMES_ABBREV=[
	"Sam.",
	"Dum.",
	"Riu.",
	"Ana.",
	"Ogr.",
	"Cut.",
	"Gia.",
	"Sim.",
	"Equ.",
	"Ele.",
	"Aed.",
	"Can."
];

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_SAMONIOS   = 0;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_DUMANIOS   = 1;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_RIUROS     = 2;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_ANAGANTIOS = 3;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_OGRONNIOS  = 4;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_CUTIOS     = 5;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_GIAMONIOS  = 6;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_SIMIVISONNA= 7;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_EQUOS      = 8;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_ELEMBIVOS  = 9;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_AEDRINIOS  =10;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_CANTLOS    =11;

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE=60;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MINUTES_IN_HOUR  =60;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.HOURS_IN_DAY     =24;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK     = 7;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS_IN_MONTH   = 4;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_YEAR   =12;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_SEASON = 3;


net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR  =net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MINUTES_IN_HOUR;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_DAY   =net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR  *net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.HOURS_IN_DAY;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_WEEK  =net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_DAY   *net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MONTH =net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_WEEK  *net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS_IN_MONTH;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_YEAR  =net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MONTH *net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_YEAR;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_SEASON=net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MONTH *net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_SEASON;

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS          = 0;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MINUTES          = 1;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.HOURS            = 2;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS             = 3;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS            = 4;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS           = 5;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.YEARS            = 6;

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SUMMER_SOLSTICE  = 1;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.FALL_EQUINOX     = 4;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WINTER_SOLSTICE  = 7;
net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SPRING_EQUINOX   =10;

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.init=function(initData)
{
	this.parts=[];
	this.callSuper(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar,"init",[initData]);
	
	return(this);
}

////

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSecondsInMinute=function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMinutesInHour  =function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MINUTES_IN_HOUR);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getHoursInDay     =function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.HOURS_IN_DAY);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getDaysInWeek     =function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getWeeksInMonth   =function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS_IN_MONTH);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMonthsInSeason =function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_SEASON);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSeasonsInYear  =function()
{
	return(
		net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_YEAR  /
		net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_SEASON
	);
};

////

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMaximumAllowableSecond=function()
{
	return(SECONDS_IN_MINUTE);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMaximumAllowableMinute=function()
{
	return(MINUTES_IN_HOUR);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMaximumAllowableHour=function()
{
	return(HOURS_IN_DAY);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMaximumAllowableDay=function()
{
	return(DAYS_IN_WEEK);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMaximumAllowableDate=function()
{
	return(DAYS_IN_WEEK*WEEKS_IN_MONTH);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMaximumAllowableWeek=function()
{
	return(WEEKS_IN_MONTH);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMaximumAllowableMonth=function()
{
	return(MONTHS_IN_YEAR);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMaximumAllowableSeason=function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SEASON_NAMES.length-1);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.toTimeString=function()
{
	return(
		this.getHours  ().padLeft(2)+":"+
		this.getMinutes().padLeft(2)
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.toDateString=function()
{
	return(
		(this.getDate()+1)+" "+
		this.getShortMonthName()+" "+
		this.getYear()
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.toLongDateString=function()
{
	return(
		(this.getDate()+1)+" "+
		this.getMonthName()+" "+
		this.getYear()
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.toShortDateString=function()
{
	return(
		(this.getDate()+1).padLeft(2)+"/"+
		(this.getMonth()+1).padLeft(2)+"/"+
		this.getYear().padLeft(4)
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.toString=function()
{
	return(
		(this.getDate()+1)+" "+
		this.getShortMonthName()+" "+
		this.getYear   ()+", "+
		this.getHours  ().padLeft(2)+":"+
		this.getMinutes().padLeft(2)
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.toLongString=function()
{
	return(
		(this.getDate()+1)+" "+
		this.getMonthName()+" "+
		this.getYear   ()+", "+
		this.getHours  ().padLeft(2)+":"+
		this.getMinutes().padLeft(2)
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.toShortString=function()
{
	return(
		(this.getDate  ()+1).padLeft(2)+"/"+
		(this.getMonth ()+1).padLeft(2)+"/"+
		this.getYear   ().padLeft(4)+" "+
		this.getHours  ().padLeft(2)+":"+
		this.getMinutes().padLeft(2)
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setTime=function(seconds)
{
	this.callSuper(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar,"setTime",[seconds]);
	this.parse();
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.parse=function()
{
	var time=this.getTime();
	
	this.parts=new Array(7);
	
	if(Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_YEAR) > 0)
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.YEARS] =Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_YEAR);
		time        -=this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.YEARS]*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_YEAR;
	}
	else
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.YEARS]=0;
	}

	if(Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MONTH) > 0)
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS] =Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MONTH);
		time         -=this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS]*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MONTH;
	}
	else
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS]=0;
	}

	if(Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_WEEK) > 0)
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS] =Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_WEEK);
		time         -=this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS]*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_WEEK;
	}
	else
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS]=0;
	}

	if(Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_DAY) > 0)
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS] =Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_DAY);
		time         -=this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS]*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_DAY;
	}
	else
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS]=0;
	}

	if(Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR) > 0)
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.HOURS] =Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR);
		time         -=this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.HOURS]*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR;
	}
	else
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.HOURS]=0;
	}

	if(Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE) > 0)
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MINUTES] =Math.floor(time/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE);
		time         -=this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MINUTES]*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE;
	}
	else
	{
		this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MINUTES]=0;
	}
	
	this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS]=Math.floor(time);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSeconds=function()
{
	return(this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setSeconds=function(seconds)
{
	this.setTime((this.getTime()-this.getSeconds())+seconds);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMinutes=function()
{
	return(this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MINUTES]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setMinutes=function(minutes)
{
	this.setTime((this.getTime()-(this.getMinutes()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE))+(minutes*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE));
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getHours=function()
{
	return(this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.HOURS]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setHours=function(hours)
{
	this.setTime((this.getTime()-(this.getHours()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR))+(hours*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR));
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getDay=function()
{
	return(this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setDay=function(day)
{
	this.setTime((this.getTime()-(this.getDay()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_DAY))+((day-1)*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_DAY));
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getDate=function()
{
    return((this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS]*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK)+this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setDate=function(day)
{
	if(Math.floor(day/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK) > 0)
	{
		this.setWeek(Math.floor(day/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK)+1);
		day -=Math.floor(day/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK)*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK;
	}
	else
	{
		this.setWeek(1);
	}
	
	this.setDay(day);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getWeek=function()
{
	return(this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setWeek=function(week)
{
	this.setTime((this.getTime()-(this.getWeek()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_WEEK))+((week-1)*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_WEEK));
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMonthName=function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_NAMES[this.getMonth()]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getShortMonthName=function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTH_NAMES_ABBREV[this.getMonth()]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getMonth=function()
{
	return(this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setMonth=function(month)
{
	this.setTime((this.getTime()-(this.getMonth()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MONTH))+((month-1)*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MONTH));
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getYear=function()
{
	return(this.parts[net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.YEARS]);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.setYear=function(year)
{
	this.setTime((this.getTime()-(this.getYear()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_YEAR))+(year*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_YEAR));
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getLunarPhase    =function()
{
	 return((this.getDay()+this.getWeek()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK)/(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.DAYS_IN_WEEK*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WEEKS_IN_MONTH));
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getLunarPhaseName=function()
{
	if(this.getDate() == 0)
	{
		return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES[0]);
	}
	else if(this.getDate() > 0 && this.getDate() < 7)
	{
		return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES[1]);
	}
	else if(this.getDate() == 7)
	{
		return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES[2]);
	}
	else if(this.getDate() > 7 && this.getDate() < 14)
	{
		return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES[3]);
	}
	else if(this.getDate() == 14)
	{
		return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES[4]);
	}
	else if(this.getDate() > 14 && this.getDate() < 21)
	{
		return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES[5]);
	}
	else if(this.getDate() == 21)
	{
		return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES[6]);
	}
	else
	{
		return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.LUNAR_PHASE_NAMES[7]);
	}
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSunrise=function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR* 6);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSunset=function()
{
	return(net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR*18);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSolarPosition=function()
{
	var tod=this.getTimeOfDayAsPercent();
	
	if(tod < 0.5)
	{
		return(tod*2);
	}
	else
	{
		return(1.0-(tod-0.5)*2);
	}
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.isTodaySummerSolstice=function()
{
	return(this.getMonth() == net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SUMMER_SOLSTICE && this.getWeek() == 2);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.isTodayWinterSolstice=function()
{
	return(this.getMonth() == net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.WINTER_SOLSTICE && this.getWeek() == 2);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.isTodaySpringEquinox =function()
{
	return(this.getMonth() == net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SPRING_EQUINOX && this.getWeek() == 2);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.isTodayFallEquinox   =function()
{
	return(this.getMonth() == net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.FALL_EQUINOX && this.getWeek() == 2);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getTimeOfDay=function()
{
	return(
		(this.getHours  ()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_HOUR  )+
		(this.getMinutes()*net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_MINUTE)+
		(this.getSeconds()                                                                                         )
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getTimeOfDayAsPercent=function()
{
	return(this.getTimeOfDay()/net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_DAY);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSeasonDetailName=function()
{
	var prefix="Late ";
	
	if(this.getSeasonProgress() < 0.34)
	{
		prefix="Early ";
	}
	else if(this.getSeasonProgress() < 0.67)
	{
		prefix="Mid-";
	}
	
    return(prefix+this.getSeasonName());
}

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSeasonName=function()
{
	return(
		net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SEASON_NAMES[
			this.getSeason()
		]
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSeason=function()
{
	return(
		Math.floor(
			this.getMonth() / 
			(
				net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_YEAR/
				(
					net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_YEAR/
					net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.MONTHS_IN_SEASON
				)
			)
		)
	);
};

net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.prototype.getSeasonProgress=function()
{
    return(
    	(
    		this.getTime() % 
    		net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_SEASON
    	) /
    	net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar.SECONDS_IN_SEASON
    );
};