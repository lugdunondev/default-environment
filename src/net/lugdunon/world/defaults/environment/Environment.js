Namespace.declare("net.lugdunon.world.defaults.environment");
Namespace.require("net.lugdunon.util.Color");
Namespace.require("net.lugdunon.world.environment.calendar.BaseCalendar");
Namespace.require("net.lugdunon.world.defaults.environment.settings.LightingDetailSetting");
Namespace.newClass("net.lugdunon.world.defaults.environment.Environment","net.lugdunon.world.environment.IEnvironment");

net.lugdunon.world.defaults.environment.Environment.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.Environment,"init",[initData]);
	
	var dnCycle   =assetManager.getImage("DN_CYCLE");
	var tmpCanvas =game.makeCanvas(24,1);
	var tmpImgData=null;
	
	if(dnCycle == null)
	{
		//ERROR
	}
	
	tmpCanvas.getContext("2d").drawImage(dnCycle,0,0);
	tmpImgData=tmpCanvas.getContext("2d").getImageData(0,0,24,1).data;
	
	this.calendar          =Namespace.instantiate("net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar").init(initData.time);
	
	this.drawLightLayer     =true;
	
	this.forceUpdate        =false;
	this.prevMinutes        = -1;
	this.currentSkyColor    =null;
	this.currentSkyAlpha    =0.0;
	this.ambientLight       =null;
	
	this.efCtxt             =game.getContext();
	this.cycleIndex         =new Array(24);
	this.lod                =0;
	this.halfLod            =0;
	this.intensity          =0.5;
	this.width              =64;
	this.height             =64;
	
	this.tempLoc            ={x:0,y:0,t:false};
	this.pGCO               =null;       
	
	this.weatherFadeDuration=2000;
	this.weather            ={};
	this.weatherIds         =[];
	
	if(game.isWebGLEnabled)
	{
		
	}
	else
	{
		for(var i=0;i<24;i++)
		{
			this.cycleIndex[i]={
				c:net.lugdunon.util.Color.toRGBAString(
					tmpImgData[(i*4)+0],
					tmpImgData[(i*4)+1],
					tmpImgData[(i*4)+2],
					tmpImgData[(i*4)+3]
				),
				r:tmpImgData[(i*4)+0],
				g:tmpImgData[(i*4)+1],
				b:tmpImgData[(i*4)+2],
				a:tmpImgData[(i*4)+3]
			};
		}
		
		this.canvas =game.makeCanvas(64,64);
		this.context=this.canvas.getContext("2d");
		
		game.minimap.setEnvironment(this.canvas);

		this.weatherCanvas =game.makeCanvas(
			game.getCanvasWidthInPixels (),
			game.getCanvasHeightInPixels()
		);
		this.weatherContext=this.weatherCanvas.getContext("2d");
	}
	
	game.addSetting(new net.lugdunon.world.defaults.environment.settings.LightingDetailSetting().init({}));
	
	this.setLOD(game.getSetting("net.lugdunon.world.defaults.environment.settings.LightingDetailSetting").getSettingValue().lod+1);
	
	this.update(this.hourLength);

	game.chunkManager.registerInstanceChangeListener("ENVIRONMENT",this);
	game.chunkManager.registerChunksUpdatedListener ("ENVIRONMENT",this);
	
	this.instanceChangeOccurred();
	
	return(this);
};

net.lugdunon.world.defaults.environment.Environment.prototype.makeWeather=function(id,clazz,initData)
{
	var fade=null;
	
	if(this.weatherIds.contains(id))
	{
		fade=this.weather[id].getFade();
		
		this.weather[id].finished();
		delete this.weather[id];
		this.weatherIds.remove(id);
	}
	
	initData.id             =id;
	
	initData.weatherCanvas  =this.weatherCanvas;
	initData.weatherContext =this.weatherContext;
	
	initData.lightMapCanvas =this.canvas;
	initData.lightMapContext=this.context;
	
	this.weather[id]        =Namespace.instantiate(clazz).init(initData);
	
	this.weatherIds.push(id);
	
	if(fade)
	{
		this.weather[id].setFade(fade);
	}
	
	this.weather[id].started();
};

net.lugdunon.world.defaults.environment.Environment.prototype.getWeather=function(id)
{
	return(this.weather[id]);
};

net.lugdunon.world.defaults.environment.Environment.prototype.hasWeather=function(id)
{
	return(this.weather[id] != null);
};

net.lugdunon.world.defaults.environment.Environment.prototype.clearWeather=function(id)
{
	//CLEAR EVERYTHING
	if(id == null)
	{
		for(var i=0;i<this.weatherIds.length;i++)
		{
			if(this.weather[this.weatherIds[i]] != null)
			{
				try
				{
					this.weather[this.weatherIds[i]].setFadeState(net.lugdunon.world.environment.weather.BaseWeather.FADE_STATE_OUT);
				}
				catch(e)
				{
					Namespace.error(e);
				}
			}
		}
	}
	//JUST CLEAR THE SPECIFIC ONE
	else
	{
		if(this.weather[id] != null)
		{
			try
			{
				this.weather[id].setFadeState(net.lugdunon.world.environment.weather.BaseWeather.FADE_STATE_OUT);
			}
			catch(e)
			{
				Namespace.error(e);
			}
		}
	}
};

net.lugdunon.world.defaults.environment.Environment.prototype.getCalendar=function()
{
	return(this.calendar);
};

net.lugdunon.world.defaults.environment.Environment.prototype.newCalendar=function(time)
{
	return(
		Namespace.instantiate(
			"net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar"
		).init(
			time
		)
	);
};

net.lugdunon.world.defaults.environment.Environment.prototype.getLOD=function()
{
	return((this.lod+1)/2);
}

net.lugdunon.world.defaults.environment.Environment.prototype.setLOD=function(lod)
{
	this.lod    =(lod*2)-1;
	this.halfLod=Math.floor(this.lod/2);
	
	this.screenConfigurationUpdated();
}


net.lugdunon.world.defaults.environment.Environment.prototype.screenConfigurationUpdated=function()
{
	if(game.isWebGLEnabled)
	{
		
	}
	else
	{
		this.width        =(game.minimap.rect.w*game.tileSetDef.chunkSize)*this.lod;
		this.height       =(game.minimap.rect.h*game.tileSetDef.chunkSize)*this.lod;
		
		this.canvas.width =this.width;
		this.canvas.height=this.height;
		this.imgData      =this.context.getImageData(0,0,this.canvas.width,this.canvas.height).data;
		
		$(this.canvas).css("width", game.minimap.canvas.width +"px");		
		$(this.canvas).css("height",game.minimap.canvas.height+"px");

		this.weatherCanvas.width =game.getCanvasWidthInPixels ();
		this.weatherCanvas.height=game.getCanvasHeightInPixels();
	}
}


net.lugdunon.world.defaults.environment.Environment.prototype.instanceChangeOccurred=function()
{
	if(game.instanceDefinition.indoor === true)
	{
		this.ambientLight=game.instanceDefinition.ambientLight;
	}
	else
	{
		this.ambientLight=null;
	}
	
	this.forceUpdate=true;
};

net.lugdunon.world.defaults.environment.Environment.prototype.chunksUpdated=function()
{
	this.instanceChangeOccurred();
};

net.lugdunon.world.defaults.environment.Environment.prototype.update=function(delta)
{
	var ls;
	var lo;
	var p ={x:0,y:0};
	
	if(this.ambientLight === null)
	{
		if((this.forceUpdate === true) || (this.prevMinutes != this.calendar.getMinutes()))
		{
			var nh=this.calendar.getHours()==23?0:this.calendar.getHours()+1;
			var fr=this.calendar.getMinutes()/60.0;
			
			this.prevMinutes      =this.calendar.getMinutes();
			this.currentSkyAlpha  =(this.cycleIndex[this.calendar.getHours()].a*(1-fr))+(this.cycleIndex[nh].a*(fr));
			this.currentSkyColor  =net.lugdunon.util.Color.toRGBAString(
				Math.round((this.cycleIndex[this.calendar.getHours()].r*(1-fr))+(this.cycleIndex[nh].r*(fr))),
				Math.round((this.cycleIndex[this.calendar.getHours()].g*(1-fr))+(this.cycleIndex[nh].g*(fr))),
				Math.round((this.cycleIndex[this.calendar.getHours()].b*(1-fr))+(this.cycleIndex[nh].b*(fr))),
				this.currentSkyAlpha
			);
			
			this.nextHour=nh;
			this.frMins  =fr;
		}
	}
	else
	{
		this.currentSkyAlpha  =this.ambientLight.a;
		this.currentSkyColor  =net.lugdunon.util.Color.toRGBAString(
			this.ambientLight.r,
			this.ambientLight.g,
			this.ambientLight.b,
			this.currentSkyAlpha
		);
	}

	this.context.fillStyle=this.currentSkyColor;
	this.context.clearRect(0,0,this.canvas.width,this.canvas.height);
	this.context.fillRect (0,0,this.canvas.width,this.canvas.height);
	
	//PLAYER
	for(var i=0;i<net.lugdunon.character.Character.ACTION_PLAY_SIZE;i++)
	{
		ls=game.player.getInventoryItem(net.lugdunon.character.Character.ACTION_PLAY_BLOCK,i);
		
		if(ls != null && ls.itemDef.isLightsource())
		{
			lo=ls.itemDef.getLightingConfig();

			if(lo != null)
			{
				net.lugdunon.util.Math.tileToRawMinimapCoords(game.player.loc,p);
				
				this.renderLight(
					p.x+(game.normalizedScreenOffset.x/game.tileSetDef.tileSize),
					p.y+(game.normalizedScreenOffset.y/game.tileSetDef.tileSize),
					lo.r,
					lo.c,
					lo.i
				);
			}
		}
	}
	
	for(var i=0;i<game.chunkManager.terrainlights.length;i++)
	{
		lo=game.chunkManager.terrainlights[i];
		
		if(lo != null && game.chunkManager.hasLineOfSight(lo.tp))
		{
			this.renderLight(
				lo.p.x,
				lo.p.y,
				lo.r,
				lo.c,
				lo.i
			);
		}
	}
	
	for(var i=0;i<game.chunkManager.lightsources.length;i++)
	{
		ls=game.chunkManager.lightsources[i];
		lo=ls.getLightingConfig();
		
		if(lo != null && game.chunkManager.hasLineOfSight(ls.loc))
		{
			net.lugdunon.util.Math.tileToRawMinimapCoords(ls.loc,p);
			
			this.renderLight(
				p.x+lo.p.x,
				p.y+lo.p.y,
				lo.r,
				lo.c,
				lo.i
			);
		}
	}
	
	//UPDATE WEATHER
	try
	{
		for(var i=0;i<this.weatherIds.length;i++)
		{
			if(this.weather[this.weatherIds[i]])
			{
				this.weather[this.weatherIds[i]].update(delta);
				
				if(this.weather[this.weatherIds[i]].getFadeState() == net.lugdunon.world.environment.weather.BaseWeather.FADE_STATE_IN)
				{
					this.weather[this.weatherIds[i]].setFade(
						this.weather[this.weatherIds[i]].getFade()+
						(delta/this.weatherFadeDuration)
					);
					
					if(this.weather[this.weatherIds[i]].getFade() >= 1.0)
					{
						this.weather[this.weatherIds[i]].setFadeState(net.lugdunon.world.environment.weather.BaseWeather.FADE_STATE_NONE);
					}
				}
				else if(this.weather[this.weatherIds[i]].getFadeState() == net.lugdunon.world.environment.weather.BaseWeather.FADE_STATE_OUT)
				{
					this.weather[this.weatherIds[i]].setFade(
						this.weather[this.weatherIds[i]].getFade()-
						(delta/this.weatherFadeDuration)
					);
					
					if(this.weather[this.weatherIds[i]].getFade() <= 0.0)
					{
						this.weather[this.weatherIds[i]].finished();
						delete this.weather[this.weatherIds[i]];
						this.weatherIds.remove(this.weatherIds[i]);
						
						i--;
					}
				}
			}
			else
			{
				this.weather[this.weatherIds[i]].finished();
				delete this.weather[this.weatherIds[i]];
				this.weatherIds.remove(this.weatherIds[i]);
				
				i--;
			}
		}
	}
	catch(e)
	{
		Namespace.debug(e);
	}
	
//	var id=this.context.getImageData(0,0,this.canvas.width,this.canvas.height);
//	
//	for(var i=3;i<id.data.length;i+=4)
//	{
//		id.data[i]=255-id.data[i];
//	}
//	
//	this.context.putImageData(id,0,0);
	
	if(this.forceUpdate === true)
	{
		this.forceUpdate=false;
	}
};

/*
 * x   - x coordinate of light's origin
 * y   - y coordinate of light's origin
 * r   - radius of light
 * c   - color of light
 * int - intensity of light
 * 
 */
net.lugdunon.world.defaults.environment.Environment.prototype.renderLight=function(x,y,r,c,int,xMod,yMod)
{
	this.renderLightToData(x,y,r,c,int,xMod,yMod);
	
	if(game.tileSetDef.mapSize == game.minimap.rect.w*game.tileSetDef.chunkSize)
	{
		if(x <= r && y <= r)
		{
			this.renderLightToData(game.tileSetDef.mapSize+x,y,                        r,c,int,xMod,yMod);
			this.renderLightToData(x,                        game.tileSetDef.mapSize+y,r,c,int,xMod,yMod);
			this.renderLightToData(game.tileSetDef.mapSize+x,game.tileSetDef.mapSize+y,r,c,int,xMod,yMod);
		}
		else if(x+r >= game.tileSetDef.mapSize && y+r >= game.tileSetDef.mapSize)
		{
			this.renderLightToData(x-game.tileSetDef.mapSize,y,                        r,c,int,xMod,yMod);
			this.renderLightToData(x,                        y-game.tileSetDef.mapSize,r,c,int,xMod,yMod);
			this.renderLightToData(x-game.tileSetDef.mapSize,y-game.tileSetDef.mapSize,r,c,int,xMod,yMod);
		}
		else if(x <= r && y+r >= game.tileSetDef.mapSize)
		{
			this.renderLightToData(game.tileSetDef.mapSize+x,y,                        r,c,int,xMod,yMod);
			this.renderLightToData(x,                        y-game.tileSetDef.mapSize,r,c,int,xMod,yMod);
			this.renderLightToData(game.tileSetDef.mapSize+x,y-game.tileSetDef.mapSize,r,c,int,xMod,yMod);
		}
		else if(x+r >= game.tileSetDef.mapSize && y <= r)
		{
			this.renderLightToData(x-game.tileSetDef.mapSize,y,                        r,c,int,xMod,yMod);
			this.renderLightToData(x,                        game.tileSetDef.mapSize+y,r,c,int,xMod,yMod);
			this.renderLightToData(x-game.tileSetDef.mapSize,game.tileSetDef.mapSize+y,r,c,int,xMod,yMod);
		}
		else
		{
			if(x <= r)
			{
				x=game.tileSetDef.mapSize+x;
				
				this.renderLightToData(x,y,r,c,int,xMod,yMod);
			}
			else if(x+r >= game.tileSetDef.mapSize)
			{
				x=x-game.tileSetDef.mapSize;
				
				this.renderLightToData(x,y,r,c,int,xMod,yMod);
			}
			
			if(y <= r)
			{
				y=game.tileSetDef.mapSize+y;
				
				this.renderLightToData(x,y,r,c,int,xMod,yMod);
			}
			else if(y+r >= game.tileSetDef.mapSize)
			{
				y=y-game.tileSetDef.mapSize;
				
				this.renderLightToData(x,y,r,c,int,xMod,yMod);
			}
		}
	}
};

net.lugdunon.world.defaults.environment.Environment.prototype.renderLightToData=function(x,y,r,c,int,xMod,yMod)
{
	//TODO: STOP LIGHT WHEN IT HITS TERRAIN LAYER ELEVATION HIGHER THAN IT'S SOURCE
	
	x =Math.floor((x*this.lod)+(xMod==null?this.halfLod:xMod));
	y =Math.floor((y*this.lod)+(yMod==null?this.halfLod:yMod));
	r*=this.lod;
	
	var i;
	var j;
	var k;
	var ik;
	var z;
	var iz;
	var d   =((r+this.lod)*2);
	var rr  =r*r;
	var p   ={x:0,y:0};
	var ct  ={x:r,y:r};
	var id  =this.context.getImageData(x-r,y-r,d,d);
	var inti=(1.0-int);
	
	for(i=0;i<d;i++)
	{
		for(j=0;j<d;j++)
		{
			p.x=i;
			p.y=j;
			z  =net.lugdunon.util.Math.distanceSquared(p,ct)/rr;
			
			if(z < 1.0)
			{
				z           =inti+(z*int);
				iz          =1-z;
				k           =(i*d+j)*4;

				id.data[k+0]=(c.r*iz)+(id.data[k+0]*z);
				id.data[k+1]=(c.g*iz)+(id.data[k+1]*z);
				id.data[k+2]=(c.b*iz)+(id.data[k+2]*z);
				
				if(this.currentSkyAlpha < 16)
				{
					id.data[k+3]=(255*iz*iz*iz*iz)+(id.data[k+3]*z);
				}
				else if(this.currentSkyAlpha < 32)
				{
					id.data[k+3]=(255*iz*iz*iz   )+(id.data[k+3]*z);
				}
				else
				{
					id.data[k+3]=(255*iz*iz      )+(id.data[k+3]*z);
				}
			}
		}
	}

	this.context.putImageData(id,x-r,y-r);
};

//net.lugdunon.world.defaults.environment.Environment.dumpCanvas;
//
//net.lugdunon.world.defaults.environment.Environment.prototype.dump=function()
//{
//	net.lugdunon.world.defaults.environment.Environment.dumpCanvas=game.makeCanvas(game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels());
//	var efCtxt=net.lugdunon.world.defaults.environment.Environment.dumpCanvas.getContext("2d");
//	efCtxt.clearRect(0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels());
//	try
//	{		
//		this.tempLoc.x=(((game.getCurrentViewRect().l+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize)-game.minimap.rect.x)*this.lod;
//		this.tempLoc.y=(((game.getCurrentViewRect().t+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize)-game.minimap.rect.y)*this.lod;
//		this.tempLoc.t=false;
//		
//		efCtxt.drawImage(
//			this.canvas,
//			this.tempLoc.x,
//			this.tempLoc.y,
//			game.screenDim.w*this.lod,
//			game.screenDim.h*this.lod,
//			0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
//		);
//		
//		if(game.tileSetDef.mapSize == game.minimap.rect.w*game.tileSetDef.chunkSize)
//		{
//			if(game.getCurrentViewRect().t >= game.getCurrentViewRect().b)
//			{
//				efCtxt.drawImage(
//					this.canvas,
//					(((game.getCurrentViewRect().l+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize)                 )*this.lod,
//					(((game.getCurrentViewRect().b+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize)-game.screenDim.h)*this.lod,
//					game.screenDim.w*this.lod,
//					game.screenDim.h*this.lod,
//					0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
//				);
//			}
//			
//			if(game.getCurrentViewRect().l >= game.getCurrentViewRect().r)
//			{
//				efCtxt.drawImage(
//					this.canvas,
//					(((game.getCurrentViewRect().r+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize)-game.screenDim.w)*this.lod,
//					(((game.getCurrentViewRect().t+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize)                 )*this.lod,
//					game.screenDim.w*this.lod,
//					game.screenDim.h*this.lod,
//					0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
//				);
//			}
//			
//			if((game.getCurrentViewRect().t >= game.getCurrentViewRect().b) && (game.getCurrentViewRect().l >= game.getCurrentViewRect().r))
//			{
//				efCtxt.drawImage(
//					this.canvas,
//					(((game.getCurrentViewRect().r+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize)-game.screenDim.w)*this.lod,
//					(((game.getCurrentViewRect().b+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize)-game.screenDim.h)*this.lod,
//					game.screenDim.w*this.lod,
//					game.screenDim.h*this.lod,
//					0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
//				);
//			}
//		}
//		else
//		{
//			if(game.minimap.rect.x*game.tileSetDef.tileSize > (game.getCurrentViewRect().l+(game.player.isPathing()?game.normalizedScreenOffset.x:0)))
//			{
//				this.tempLoc.t=true;
//				this.tempLoc.x=((game.tileSetDef.mapSize-game.minimap.rect.x)+((game.getCurrentViewRect().l+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize))*this.lod;
//			}
//			
//			if(game.minimap.rect.y*game.tileSetDef.tileSize > (game.getCurrentViewRect().t+(game.player.isPathing()?game.normalizedScreenOffset.y:0)))
//			{
//				this.tempLoc.t=true;
//				this.tempLoc.y=((game.tileSetDef.mapSize-game.minimap.rect.y)+((game.getCurrentViewRect().t+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize))*this.lod;
//			}
//			
//			if(this.tempLoc.t)
//			{
//				efCtxt.drawImage(
//					this.canvas,
//					this.tempLoc.x,
//					this.tempLoc.y,
//					game.screenDim.w*this.lod,
//					game.screenDim.h*this.lod,
//					0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
//				);
//			}
//		}
//	}
//	catch(e)
//	{
//		console.log(e);
//	}
//};

net.lugdunon.world.defaults.environment.Environment.prototype.draw=function(delta)
{
	if(game.isWebGLEnabled)
	{
		
	}
	else
	{
		//WEATHER
		try
		{
			this.weatherContext.clearRect(0,0,this.weatherCanvas.width,this.weatherCanvas.height);
			
			for(var i=0;i<this.weatherIds.length;i++)
			{
				this.weather[this.weatherIds[i]].drawPreLighting(delta);
			}
			
			this.efCtxt.drawImage(
				this.weatherCanvas,
				0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels(),
				0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
			);
		}
		catch(e)
		{
			Namespace.debug(e);
		}
		
		if(this.drawLightLayer)
		{
			this.pGCO=this.efCtxt.globalCompositeOperation;
			
			try
			{		
				this.tempLoc.x=(((game.getCurrentViewRect().l+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize)-game.minimap.rect.x)*this.lod;
				this.tempLoc.y=(((game.getCurrentViewRect().t+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize)-game.minimap.rect.y)*this.lod;
				this.tempLoc.t=false;
	
				this.efCtxt.globalCompositeOperation="multiply";
				
				this.efCtxt.drawImage(
					this.canvas,
					this.tempLoc.x,
					this.tempLoc.y,
					game.screenDim.w*this.lod,
					game.screenDim.h*this.lod,
					0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
				);
				
				if(game.tileSetDef.mapSize == game.minimap.rect.w*game.tileSetDef.chunkSize)
				{
					if(game.getCurrentViewRect().t >= game.getCurrentViewRect().b)
					{
						this.efCtxt.drawImage(
							this.canvas,
							(((game.getCurrentViewRect().l+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize)                 )*this.lod,
							(((game.getCurrentViewRect().b+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize)-game.screenDim.h)*this.lod,
							game.screenDim.w*this.lod,
							game.screenDim.h*this.lod,
							0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
						);
					}
					
					if(game.getCurrentViewRect().l >= game.getCurrentViewRect().r)
					{
						this.efCtxt.drawImage(
							this.canvas,
							(((game.getCurrentViewRect().r+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize)-game.screenDim.w)*this.lod,
							(((game.getCurrentViewRect().t+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize)                 )*this.lod,
							game.screenDim.w*this.lod,
							game.screenDim.h*this.lod,
							0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
						);
					}
					
					if((game.getCurrentViewRect().t >= game.getCurrentViewRect().b) && (game.getCurrentViewRect().l >= game.getCurrentViewRect().r))
					{
						this.efCtxt.drawImage(
							this.canvas,
							(((game.getCurrentViewRect().r+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize)-game.screenDim.w)*this.lod,
							(((game.getCurrentViewRect().b+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize)-game.screenDim.h)*this.lod,
							game.screenDim.w*this.lod,
							game.screenDim.h*this.lod,
							0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
						);
					}
				}
				else
				{
					if(game.minimap.rect.x*game.tileSetDef.tileSize > (game.getCurrentViewRect().l+(game.player.isPathing()?game.normalizedScreenOffset.x:0)))
					{
						this.tempLoc.t=true;
						this.tempLoc.x=((game.tileSetDef.mapSize-game.minimap.rect.x)+((game.getCurrentViewRect().l+(game.player.isPathing()?game.normalizedScreenOffset.x:0))/game.tileSetDef.tileSize))*this.lod;
					}
					
					if(game.minimap.rect.y*game.tileSetDef.tileSize > (game.getCurrentViewRect().t+(game.player.isPathing()?game.normalizedScreenOffset.y:0)))
					{
						this.tempLoc.t=true;
						this.tempLoc.y=((game.tileSetDef.mapSize-game.minimap.rect.y)+((game.getCurrentViewRect().t+(game.player.isPathing()?game.normalizedScreenOffset.y:0))/game.tileSetDef.tileSize))*this.lod;
					}
					
					if(this.tempLoc.t)
					{
						this.efCtxt.drawImage(
							this.canvas,
							this.tempLoc.x,
							this.tempLoc.y,
							game.screenDim.w*this.lod,
							game.screenDim.h*this.lod,
							0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
						);
					}
				}
			}
			catch(e)
			{
				;
			}
	
			this.efCtxt.globalCompositeOperation=this.pGCO;
		}
		
		//WEATHER
		try
		{
			this.weatherContext.clearRect(0,0,this.weatherCanvas.width,this.weatherCanvas.height);
			
			for(var i=0;i<this.weatherIds.length;i++)
			{
				this.weather[this.weatherIds[i]].drawPostLighting(delta);
			}
			
			this.efCtxt.drawImage(
				this.weatherCanvas,
				0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels(),
				0,0,game.getCanvasWidthInPixels(),game.getCanvasHeightInPixels()
			);
		}
		catch(e)
		{
			Namespace.debug(e);
		}
	}
};

net.lugdunon.world.defaults.environment.Environment.prototype.setTime=function(time)
{
	this.calendar.setTime(time);
};