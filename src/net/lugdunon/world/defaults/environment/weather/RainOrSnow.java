package net.lugdunon.world.defaults.environment.weather;

import net.lugdunon.world.environment.hz.HardinessZone;
import net.lugdunon.world.environment.weather.BaseWeather;

import org.json.JSONObject;

public class RainOrSnow extends BaseWeather
{
	private CommandableWeather currentSubWeather;
	
	public RainOrSnow(String id, HardinessZone hardinessZone, JSONObject o)
    {
	    super(id, hardinessZone, o);
    }
	
	////

	@Override
	public String getName       ()
	{
		return("RainOrSnow ("+currentSubWeather.getName       ()+")");
	}

	@Override
	public String getDescription()
	{
		return("RainOrSnow ("+currentSubWeather.getDescription()+")");
	}
	
	////

	@Override
	public double getPrecipitationAmount()
	{
		return((currentSubWeather==null)?super.getPrecipitationAmount():currentSubWeather.getPrecipitationAmount());
	}

	@Override
	public boolean isSnowing()
	{
		return((currentSubWeather==null)?super.isSnowing():currentSubWeather.isSnowing());
	}
	
	////

	private void checkForTemperatureTransition(double temp)
    {
	    if((temp > 0.0) && (currentSubWeather == null || currentSubWeather instanceof Snow))
		{ 
	    	if(currentSubWeather != null)
	    	{
	    		currentSubWeather.finished(temp);
	    	}
	    	
			currentSubWeather=new Rain(getId(),getHardinessZone(),getArgs());
			currentSubWeather.started (temp                                );
		}
		else if((temp <= 0.0) && (currentSubWeather == null || currentSubWeather instanceof Rain))
		{
	    	if(currentSubWeather != null)
	    	{
	    		currentSubWeather.finished(temp);
	    	}
	    	
			currentSubWeather=new Snow(getId(),getHardinessZone(),getArgs());
			currentSubWeather.started (temp                                );
		}
    }
	
	////

	@Override
    public void update(double temp, long delta)
    {
		super.update(temp, delta);
		
		checkForTemperatureTransition(temp);
		
		currentSubWeather.update(temp, delta);
    }

	@Override
    public void started(double temp)
    {
		checkForTemperatureTransition(temp);
    }

	@Override
    public void finished(double temp)
    {
		if(currentSubWeather != null)
		{
			currentSubWeather.finished(temp);
		}
    }
	
	////
	
	@Override
    public String getClientSideImplClass()
    {
	    return(currentSubWeather.getClientSideImplClass());
    }

	@Override
    public JSONObject getClientSideImplArgs()
    {
	    return(currentSubWeather.getClientSideImplArgs());
    }
}
