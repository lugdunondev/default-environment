package net.lugdunon.world.defaults.environment.weather;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.command.ex.IsNotServerInvokedCommand;
import net.lugdunon.state.State;
import net.lugdunon.world.environment.hz.HardinessZone;
import net.lugdunon.world.environment.weather.BaseWeather;

import org.json.JSONObject;

public abstract class CommandableWeather extends BaseWeather
{
	public static final int COMMAND_MODE_TOTAL_UPDATE  =0;
	public static final int COMMAND_MODE_WEATHER_START =1;
	public static final int COMMAND_MODE_WEATHER_FINISH=2;
	public static final int COMMAND_MODE_PLAYER_ALTER  =3;
	
	public CommandableWeather(String id, HardinessZone hardinessZone, JSONObject o)
    {
	    super(id,hardinessZone,o);
    }
	
	////

	@Override
    public void started(double temp)
    {
        try
        {
			CommandProperties props=new CommandProperties();
			
			props.setInt       ("mode",         COMMAND_MODE_WEATHER_START );
			props.setInt       ("hardinessZone",hardinessZone.getId      ());
			props.setString    ("weatherId",    getId                    ());
			props.setString    ("implClass",    getClientSideImplClass   ());
			props.setJSONObject("implArgs",     getClientSideImplArgs    ());
			
            State.instance().getGame().addIncomingRequest(
            	"CORE.COMMAND.ENVIRONMENT.MANAGE.WEATHER",
            	props
            );
        }
        catch (CommandNotSupportedException e)
        {
            e.printStackTrace();
        }
        catch (IsNotServerInvokedCommand e)
        {
            e.printStackTrace();
        }
    }

	@Override
    public void finished(double temp)
    {
        try
        {
			CommandProperties props=new CommandProperties();

			props.setInt       ("mode",         COMMAND_MODE_WEATHER_FINISH);
			props.setInt       ("hardinessZone",hardinessZone.getId      ());
			props.setString    ("weatherId",    getId                    ());
			
            State.instance().getGame().addIncomingRequest(
            	"CORE.COMMAND.ENVIRONMENT.MANAGE.WEATHER",
            	props
            );
        }
        catch (CommandNotSupportedException e)
        {
            e.printStackTrace();
        }
        catch (IsNotServerInvokedCommand e)
        {
            e.printStackTrace();
        }
    }
}
