Namespace.declare ("net.lugdunon.world.defaults.environment.weather");
Namespace.newClass("net.lugdunon.world.defaults.environment.weather.Fog","net.lugdunon.world.environment.weather.BaseWeather");

net.lugdunon.world.defaults.environment.weather.Fog.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.weather.Fog,"init",[initData]);

	this.fogLayerOne=assetManager.getImage("FOG_LAYER_ONE");
	this.fogLayerTwo=assetManager.getImage("FOG_LAYER_TWO");
	this.fogDensity =0.75;
	this.fogDrift   =
	{
		t :{x:0,y:0},
		l :{x:0,y:0},
		
		tv:{x: 0.125,y: 0.125},//{x:(Math.random()*2.0)-1.0,y:(Math.random()*2.0)-1.0},
		lv:{x:-0.250,y:-0.250} //{x:(Math.random()*2.0)-1.0,y:(Math.random()*2.0)-1.0}
	};
	
	return(this);
}

net.lugdunon.world.defaults.environment.weather.Fog.prototype.getAlphaAlteration=function()
{
	return(this.fogDensity);
}

net.lugdunon.world.defaults.environment.weather.Fog.prototype.updateWeather=function(delta)
{
	this.fogDrift.t.x+=this.fogDrift.tv.x;
	this.fogDrift.t.y+=this.fogDrift.tv.y;
	this.fogDrift.l.x+=this.fogDrift.lv.x;
	this.fogDrift.l.y+=this.fogDrift.lv.y;
	
	if(this.fogDrift.t.x > 511)
	{
		this.fogDrift.t.x=0+(this.fogDrift.t.x-511);
	}
	else if(this.fogDrift.t.x < 0)
	{
		this.fogDrift.t.x=511-this.fogDrift.t.x;
	}
	
	if(this.fogDrift.t.y > 511)
	{
		this.fogDrift.t.y=0+(this.fogDrift.t.y-511);
	}
	else if(this.fogDrift.t.y < 0)
	{
		this.fogDrift.t.y=511-this.fogDrift.t.y;
	}
	
	////
	
	if(this.fogDrift.l.x > 511)
	{
		this.fogDrift.l.x=0+(this.fogDrift.l.x-511);
	}
	else if(this.fogDrift.l.x < 0)
	{
		this.fogDrift.l.x=511-this.fogDrift.l.x;
	}
	
	if(this.fogDrift.l.y > 511)
	{
		this.fogDrift.l.y=0+(this.fogDrift.l.y-511);
	}
	else if(this.fogDrift.l.y < 0)
	{
		this.fogDrift.l.y=511-this.fogDrift.l.y;
	}
}

net.lugdunon.world.defaults.environment.weather.Fog.prototype.drawPreLightingWeather=function(delta)
{
	for(var i=-1024;i<this.weatherCanvas.width+512;i+=512)
	{
		for(var j=-1024;j<this.weatherCanvas.height+512;j+=512)
		{
			this.weatherContext.drawImage(
				this.fogLayerOne,
				
				i+this.fogDrift.t.x+this.screenShift.x,
				j+this.fogDrift.t.y+this.screenShift.y,
				
				512,
				512
			);
		}
	}
	
	////

	for(var i=-1024;i<this.weatherCanvas.width+512;i+=512)
	{
		for(var j=-1024;j<this.weatherCanvas.height+512;j+=512)
		{
			this.weatherContext.drawImage(
				this.fogLayerTwo,
				
				i+this.fogDrift.l.x+this.screenShift.x,
				j+this.fogDrift.l.y+this.screenShift.y,
				
				512,
				512
			);
		}
	}
}