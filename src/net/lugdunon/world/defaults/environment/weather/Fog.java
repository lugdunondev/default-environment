package net.lugdunon.world.defaults.environment.weather;

import net.lugdunon.world.environment.hz.HardinessZone;

import org.json.JSONObject;

public class Fog extends CommandableWeather
{
	public Fog(String id, HardinessZone hardinessZone, JSONObject o)
    {
	    super(id, hardinessZone, o);
    }
	
	////

	@Override
	public String getName       ()
	{
		return("Fog");
	}

	@Override
	public String getDescription()
	{
		return("Thick fog blankets the land.");
	}
	
	////

	@Override
	public double getPrecipitationAmount()
	{
		return(0.25);
	}

	@Override
    public void update(double temp, long delta)
    {
		super.update(temp, delta);
    }
}
