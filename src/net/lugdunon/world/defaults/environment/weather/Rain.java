package net.lugdunon.world.defaults.environment.weather;

import java.util.ArrayList;
import java.util.List;

import net.lugdunon.Server;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.math.Location;
import net.lugdunon.math.Point;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.state.Account;
import net.lugdunon.state.State;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.world.biome.Biome;
import net.lugdunon.world.environment.IEnvironment;
import net.lugdunon.world.environment.calendar.BaseCalendar;
import net.lugdunon.world.environment.hz.HardinessZone;
import net.lugdunon.world.instance.Instance;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Rain extends CommandableWeather
{
	private static final Logger LOGGER=LoggerFactory.getLogger(Rain.class);
	
	private long         lastLightningStrike;
	private long         lightningFrequency;
	
	private long         currentTime;
	
	private IEnvironment environment;
	private BaseCalendar calendar;
	
	public Rain(String id, HardinessZone hardinessZone, JSONObject o)
    {
	    super(id, hardinessZone, o);
	    
	    if(!WorldGenerator.isNoState())
	    {
	    	setLightningFrequency();
	    	
	    	environment        =State.instance().getWorld().getSubsystems().getEnvironment();
	    	calendar           =environment.getCalendar();
	    	lastLightningStrike=calendar.getTime()/calendar.getSecondsInMinute();
	    }
    }

	private Point findLightningLocation(Instance instance)
    {
		int   s=instance.getTerrain().getSize();
		Point p=new Point(-1,-1);
		
		for(int i=0;i<32;i++)
		{
			p.setX(WorldGenerator.getRandom().nextInt(s));
			p.setY(WorldGenerator.getRandom().nextInt(s));

			if(
				instance.getTerrain().getBiome(
					instance.getTerrain().getBiomeAt(
						p.getX(),
						p.getY()
					)
				).getHardinessZone() == getHardinessZone().getId()
			)
			{
				return(p);
			}
		}
		
		return(null);
    }
	
	private void setLightningFrequency()
	{
		lightningFrequency=WorldGenerator.getRandom().nextInt(5)+5;
	}
	
	////

	@Override
	public String getName       ()
	{
		return("Rain");
	}

	@Override
	public String getDescription()
	{
		return("Rain pours down upon the earth.");
	}
	
	////

	@Override
	public double getPrecipitationAmount()
	{
		return(1.0);
	}

	@Override
    public void update(double temp, long delta)
    {
		super.update(temp, delta);
		
		currentTime=calendar.getTime()/calendar.getSecondsInMinute();
		
		if((currentTime-lastLightningStrike) > lightningFrequency)
		{
			lastLightningStrike=currentTime;
			
			triggerOnClients();
		}
    }

	private void triggerOnClients()
    {
		List<Instance>        activeInstances  =State.instance().getWorld().listActiveInstances();
	    HardinessZone         hardinessZone    =getHardinessZone();
	    boolean               instanceHasHZ    =false;
	    List<String>          accounts         =new ArrayList<String>();
		Point                 lightningLocation;
	    
	    for(Instance instance:activeInstances)
	    {
	    	instanceHasHZ=false;
	    	
	    	for(byte biomeId:instance.getTerrain().listBiomeIds())
	    	{
	    		if(instance.getTerrain().getBiome(biomeId).getHardinessZone() == hardinessZone.getId())
	    		{
	    			instanceHasHZ=true;
	    			break;
	    		}
	    	}
	    	
	    	if(instanceHasHZ)
	    	{
		    	lightningLocation=findLightningLocation(instance);
		    	
		    	if(lightningLocation != null)
		    	{
			    	accounts.clear();
			    	
			    	LOGGER.info("Lightning in instance: "+instance.getInstanceId()+", hardiness zone: "+hardinessZone.getId()+" @"+lightningLocation+".");
	
		    		//TRIGGER LIGHTNING EVENT
		            environment.triggerEnvironmentEvent("LIGHTNING.STRIKE",new Location(instance.getInstanceId(),lightningLocation));
		    		
			    	for(PlayerCharacter pc:State.instance().listActiveCharactersInInstance(instance.getInstanceId()))
			    	{
			    		Biome biome=null;
			    		
			    		if(pc.getAccount().getGameMode() == Account.MODE_PLAY)
			    		{
			    			biome=instance.getTerrain().getBiome(
			    				instance.getTerrain().getBiomeAt(
			    					pc.getLocation().getX(),
			    					pc.getLocation().getY()
			    				)
			    			);
			    		}
			    		else if(pc.getAccount().getGameMode() == Account.MODE_EDIT)
			    		{
			    			biome=instance.getTerrain().getBiome(
			    				instance.getTerrain().getBiomeAt(
			    					pc.getAccount().getEditingLocation().getX(),
			    					pc.getAccount().getEditingLocation().getY()
			    				)
			    			);
			    		}
			    		
			    		// CHARACTER IS IN BIOME WITH WEATHER'S ASSOCIATED HARDINESS ZONE
			    		if((biome != null) && (biome.getHardinessZone() == hardinessZone.getId()))
			    		{
			    			accounts.add(pc.getAccount().getAccountName());
			    		}
			    	}
	
			    	if(Server.hasStarted() && accounts.size() > 0)
			    	{
			    		try
			    		{
							CommandProperties props=new CommandProperties();
				
							props.set      ("accounts",accounts);
							props.setString(
								"code",
								"if(game.environment && game.environment.hasWeather('"+getId()+"'))"+
								"{"+
									"game.environment.getWeather('"+
									getId()+
									"').triggerLightning({x:"+
									lightningLocation.getX()+
									",y:"+
									lightningLocation.getY()+
									"})"+
								"}"
							);
							
					        State.instance().getGame().addIncomingRequest(
					    		"CORE.COMMAND.CLIENT.EXECUTION",
					    		props
					        );
			    		}
			    		catch(Exception e)
			    		{
			    			e.printStackTrace();;
			    		}
			    	}
		    	}
	    	}
	    }
    }
}