Namespace.declare ("net.lugdunon.world.defaults.environment.weather");
Namespace.newClass("net.lugdunon.world.defaults.environment.weather.Rain","net.lugdunon.world.environment.weather.BaseWeather");

net.lugdunon.world.defaults.environment.weather.Rain.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.weather.Rain,"init",[initData]);

	var rain              =assetManager.getImage("PRECIPITATION");
	this.lightning        =assetManager.getImage("LIGHTNING"    );
	this.lightningOffset  ={x:269,y:471};
	this.lightningLocation={x:  0,y:  0};
	this.lightningAlpha   =1.0;
	this.rainSound        =assetManager.getAudio("RAIN"         );
	this.rainImageOne     =game.makeCanvas(512,512);
	this.rainImageTwo     =game.makeCanvas(512,512);
	this.rainDrift=
	{
		s :{x:0,y:0},
		l :{x:0,y:0},
		sv:{x:0,y:1},
		lv:{x:0,y:2}
	};

	this.rainImageOne.getContext("2d").clearRect(0,0,512,512);
	this.rainImageTwo.getContext("2d").clearRect(0,0,512,512);
	
	for(var i=0;i<512;i+=64)
	{
		for(var j=0;j<512;j+=64)
		{
			this.rainImageOne.getContext("2d").drawImage(rain, 0,0,64,64,i,j,64,64);
			this.rainImageTwo.getContext("2d").drawImage(rain,64,0,64,64,i,j,64,64);
		}
	}
	
	this.accLightningDelta  =    0;
	this.lightningDuration  =    0;
	this.lightningInProgress=false;
	this.isNearLightning    =false;
	
	this.tempLightMapCanvas   =game.makeCanvas(this.lightMapCanvas.width,this.lightMapCanvas.height);
	this.tempLightMapContext  =this.tempLightMapCanvas.getContext("2d");
	
	return(this);
}

net.lugdunon.world.defaults.environment.weather.Rain.prototype.getSoundEffectMaxVolume=function()
{
	return(0.5);
};

net.lugdunon.world.defaults.environment.weather.Rain.prototype.getSoundEffect=function()
{
	return(this.rainSound);
};

net.lugdunon.world.defaults.environment.weather.Rain.prototype.triggerLightning=function(loc)
{
	if(net.lugdunon.util.Math.inRangeOfPlayerLocation(loc,256))
	{
		this.isNearLightning    =net.lugdunon.util.Math.inRangeOfPlayerLocation(loc,64);
		this.lightningLocation  =loc;
		this.lightningDuration  =this.isNearLightning?750:500;
		this.accLightningDelta  =0;
		this.lightningInProgress=true;
	}
};

net.lugdunon.world.defaults.environment.weather.Rain.prototype.updateLightMap=function(delta)
{
	var gAlpha  =1.0;
	var alpha;
	var duration;
	var sound;
	
	if(this.isNearLightning)
	{
		alpha   =[0.25,0.50,0.75];
		duration=[750,562,375,187];
		sound   ="NEAR_THUNDER";
	}
	else
	{
		alpha   =[0.50,0.75,1.00];
		duration=[500,375,250,125];
		sound   ="THUNDER";
	}
	
	if(this.accLightningDelta > duration[0])
	{
		game.playSound(sound,game.getSoundVolume());
		this.lightningInProgress=false;
	}
	
	if(this.accLightningDelta < duration[3])
	{
		gAlpha =alpha[1];
		gAlpha+=alpha[0]*(this.accLightningDelta/duration[3]);
	}
	else if(this.accLightningDelta < duration[2])
	{
		gAlpha =alpha[1];
		gAlpha+=alpha[0]*((this.accLightningDelta-duration[3])/duration[3]);
	}
	else if(this.accLightningDelta < duration[1])
	{
		gAlpha =alpha[0];
		gAlpha+=alpha[1]*((this.accLightningDelta-duration[1])/duration[3]);
	}
	else
	{
		gAlpha =alpha[0];
		gAlpha+=alpha[2]-(alpha[1]*((this.accLightningDelta-duration[1])/duration[3]));
	}
	
	this.lightningAlpha=1.0-gAlpha;

	this.tempLightMapContext.clearRect(0,0,this.lightMapCanvas.width,this.lightMapCanvas.height);
	this.tempLightMapContext.globalAlpha=gAlpha;
	this.tempLightMapContext.drawImage(this.lightMapCanvas,0,0,this.lightMapCanvas.width,this.lightMapCanvas.height);
	this.tempLightMapContext.globalAlpha=1.0;

	this.lightMapContext.clearRect(0,0,this.lightMapCanvas.width,this.lightMapCanvas.height);
	this.lightMapContext.drawImage(this.tempLightMapCanvas,0,0,this.lightMapCanvas.width,this.lightMapCanvas.height);
};


//var lightningProgress=this.accLightningDelta-this.lightningFrequency;
//var alpha            =0.5;
//
//if(lightningProgress > this.lightningDuration)
//{
//	this.lightningFrequency=Math.floor((Math.random()*10000)+10000);
//	this.accLightningDelta =0;
//	
//	game.playSound("THUNDER");
//}
//
//if(lightningProgress < 125)
//{
//	alpha =0.75;
//	alpha+=0.25*(lightningProgress/125);
//}
//else if(lightningProgress < 250)
//{
//	alpha =0.75;
//	alpha+=1.0-(0.25*((lightningProgress-125)/125));
//}
//else if(lightningProgress < 375)
//{
//	alpha+=0.5*((lightningProgress-375)/125);
//}
//else
//{
//	alpha+=1.0-(0.5*((lightningProgress-375)/125));
//}

net.lugdunon.world.defaults.environment.weather.Rain.prototype.updateWeather=function(delta)
{
	this.rainDrift.s.x+=this.rainDrift.sv.x;
	this.rainDrift.s.y+=this.rainDrift.sv.y;
	
	this.rainDrift.l.x+=this.rainDrift.lv.x;
	this.rainDrift.l.y+=this.rainDrift.lv.y;
	
	if(this.rainDrift.s.x > 511)
	{
		this.rainDrift.s.x=0+(this.rainDrift.s.x-511);
	}
	else if(this.rainDrift.s.x < 0)
	{
		this.rainDrift.s.x=511-this.rainDrift.s.x;
	}
	
	if(this.rainDrift.s.y > 511)
	{
		this.rainDrift.s.y=0+(this.rainDrift.s.y-511);
	}
	else if(this.rainDrift.s.y < 0)
	{
		this.rainDrift.s.y=511-this.rainDrift.s.y;
	}
	
	if(this.rainDrift.l.x > 511)
	{
		this.rainDrift.l.x=0+(this.rainDrift.l.x-511);
	}
	else if(this.rainDrift.l.x < 0)
	{
		this.rainDrift.l.x=511-this.rainDrift.l.x;
	}
	
	if(this.rainDrift.l.y > 511)
	{
		this.rainDrift.l.y=0+(this.rainDrift.l.y-511);
	}
	else if(this.rainDrift.l.y < 0)
	{
		this.rainDrift.l.y=511-this.rainDrift.l.y;
	}
	
	if(this.lightningInProgress)
	{
		this.accLightningDelta+=delta;
		
		this.updateLightMap(delta);
	}
}

net.lugdunon.world.defaults.environment.weather.Rain.prototype.drawPreLightingWeather=function(delta)
{
	for(var i=-1024;i<this.weatherCanvas.width+512;i+=512)
	{
		for(var j=-1024;j<this.weatherCanvas.height+512;j+=512)
		{
			this.weatherContext.drawImage(
				this.rainImageOne,
				
				i+this.rainDrift.s.x+this.screenShift.x,
				j+this.rainDrift.s.y+this.screenShift.y,
				
				512,
				512
			);
		}
	}

	for(var i=-1024;i<this.weatherCanvas.width+512;i+=512)
	{
		for(var j=-1024;j<this.weatherCanvas.height+512;j+=512)
		{
			this.weatherContext.drawImage(
				this.rainImageTwo,
				
				i+this.rainDrift.l.x+this.screenShift.x,
				j+this.rainDrift.l.y+this.screenShift.y,
				
				512,
				512
			);
		}
	}
}

net.lugdunon.world.defaults.environment.weather.Rain.prototype.drawPostLightingWeather=function(delta)
{
	if(this.lightningInProgress)
	{
		var ll    =net.lugdunon.util.Math.tileToScreenCoords(this.lightningLocation);
		var gAlpha=this.weatherContext.globalAlpha;
		
		this.weatherContext.globalAlpha=this.lightningAlpha;

		ll.x+=(game.tileSetDef.tileSize/2)-this.lightningOffset.x;
		ll.y+=(game.tileSetDef.tileSize/2)-this.lightningOffset.y;
		
		this.weatherContext.drawImage(
			this.lightning,
			
			ll.x,
			ll.y,
			
			512,
			512
		);
		
		this.weatherContext.globalAlpha=gAlpha;
	}
}