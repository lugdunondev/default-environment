package net.lugdunon.world.defaults.environment.weather;

import net.lugdunon.world.environment.hz.HardinessZone;

import org.json.JSONObject;

public class Snow extends CommandableWeather
{
	public Snow(String id, HardinessZone hardinessZone, JSONObject o)
    {
	    super(id, hardinessZone, o);
    }
	
	////

	@Override
	public String getName       ()
	{
		return("Snow");
	}

	@Override
	public String getDescription()
	{
		return("Snow falls to cover the land in white.");
	}
	
	////

	@Override
	public boolean isSnowing()
	{
		return(true);
	}

	@Override
    public void update(double temp, long delta)
    {
		super.update(temp, delta);
    }
}
