Namespace.declare ("net.lugdunon.world.defaults.environment.weather");
Namespace.newClass("net.lugdunon.world.defaults.environment.weather.Snow","net.lugdunon.world.environment.weather.BaseWeather");

net.lugdunon.world.defaults.environment.weather.Snow.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.weather.Snow,"init",[initData]);

	var snow         =assetManager.getImage("PRECIPITATION");
	this.snowImageOne=game.makeCanvas(512,512);
	this.snowImageTwo=game.makeCanvas(512,512);
	this.snowDrift=
	{
		s :{x:0,y:0   },
		l :{x:0,y:0   },
		sv:{x:0,y:0.25},
		lv:{x:0,y:0.50}
	};

	this.snowImageOne.getContext("2d").clearRect(0,0,512,512);
	this.snowImageTwo.getContext("2d").clearRect(0,0,512,512);
	
	for(var i=0;i<512;i+=64)
	{
		for(var j=0;j<512;j+=64)
		{
			this.snowImageOne.getContext("2d").drawImage(snow,128,0,64,64,i,j,64,64);
			this.snowImageTwo.getContext("2d").drawImage(snow,192,0,64,64,i,j,64,64);
		}
	}
	
	return(this);
}

net.lugdunon.world.defaults.environment.weather.Snow.prototype.updateWeather=function(delta)
{
	this.snowDrift.s.x+=this.snowDrift.sv.x;
	this.snowDrift.s.y+=this.snowDrift.sv.y;
	
	this.snowDrift.l.x+=this.snowDrift.lv.x;
	this.snowDrift.l.y+=this.snowDrift.lv.y;
	
	if(this.snowDrift.s.x > 511)
	{
		this.snowDrift.s.x=0+(this.snowDrift.s.x-511);
	}
	else if(this.snowDrift.s.x < 0)
	{
		this.snowDrift.s.x=511-this.snowDrift.s.x;
	}
	
	if(this.snowDrift.s.y > 511)
	{
		this.snowDrift.s.y=0+(this.snowDrift.s.y-511);
	}
	else if(this.snowDrift.s.y < 0)
	{
		this.snowDrift.s.y=511-this.snowDrift.s.y;
	}
	
	if(this.snowDrift.l.x > 511)
	{
		this.snowDrift.l.x=0+(this.snowDrift.l.x-511);
	}
	else if(this.snowDrift.l.x < 0)
	{
		this.snowDrift.l.x=511-this.snowDrift.l.x;
	}
	
	if(this.snowDrift.l.y > 511)
	{
		this.snowDrift.l.y=0+(this.snowDrift.l.y-511);
	}
	else if(this.snowDrift.l.y < 0)
	{
		this.snowDrift.l.y=511-this.snowDrift.l.y;
	}
}

net.lugdunon.world.defaults.environment.weather.Snow.prototype.drawPreLightingWeather=function(delta)
{
	for(var i=-1024;i<this.weatherCanvas.width+512;i+=512)
	{
		for(var j=-1024;j<this.weatherCanvas.height+512;j+=512)
		{
			this.weatherContext.drawImage(
				this.snowImageOne,
				
				i+this.snowDrift.s.x+this.screenShift.x,
				j+this.snowDrift.s.y+this.screenShift.y,
				
				512,
				512
			);
		}
	}

	for(var i=-1024;i<this.weatherCanvas.width+512;i+=512)
	{
		for(var j=-1024;j<this.weatherCanvas.height+512;j+=512)
		{
			this.weatherContext.drawImage(
				this.snowImageTwo,
				
				i+this.snowDrift.l.x+this.screenShift.x,
				j+this.snowDrift.l.y+this.screenShift.y,
				
				512,
				512
			);
		}
	}
}