package net.lugdunon.world.defaults.environment;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.command.ex.IsNotServerInvokedCommand;
import net.lugdunon.math.Location;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.server.worldgen.ex.WorldIdNotSetException;
import net.lugdunon.state.State;
import net.lugdunon.state.SubsystemBase;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.world.biome.Biome;
import net.lugdunon.world.defaults.environment.calendar.SimpleGaulishCalendar;
import net.lugdunon.world.defaults.environment.weather.CommandableWeather;
import net.lugdunon.world.environment.IEnvironment;
import net.lugdunon.world.environment.calendar.BaseCalendar;
import net.lugdunon.world.environment.climate.BaseClimateInformation;
import net.lugdunon.world.environment.event.IEnvironmentListener;
import net.lugdunon.world.environment.hz.HardinessZone;
import net.lugdunon.world.environment.hz.TemperatureGradient;
import net.lugdunon.world.environment.weather.BaseWeather;
import net.lugdunon.world.environment.weather.BaseWeatherForecast;
import net.lugdunon.world.instance.Instance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Environment extends SubsystemBase implements IEnvironment
{
	private static final String[] DEFAULT_CONFIGS=new String[]{"env"};
	private static final long     DEFAULT_PUI    =5000;
	
	private long                                         secondLength;
	private long                                         accumulatedDelta;
	private BaseCalendar                                 calendar;
	private HardinessZone[]                              hardinessZones;
	private int                                          currentSeason;
	private int                                          tempMapGradient;
	private double[]                                     temperatureMap;
	private String[]                                     seasonalPlaceableItemStatePrefixes;
	private long                                         seasonalPlaceableItemUpdateInterval;
	private Map<String,Map<String,IEnvironmentListener>> environmentListeners;
	
	public Environment() throws WorldIdNotSetException
	{
		JSONObject cfg=getConfigObject("env");
		
		calendar                           =new SimpleGaulishCalendar(0);
		secondLength                       =50;
		accumulatedDelta                   = 0;
		tempMapGradient                    = 8;
		currentSeason                      =-1;
		seasonalPlaceableItemUpdateInterval=DEFAULT_PUI;
		environmentListeners               =new HashMap<String,Map<String,IEnvironmentListener>>();

		if(cfg != null)
		{
			if(cfg.has("secondLength"))
			{
				try
                {
	                secondLength=cfg.getLong("secondLength");
                }
                catch (JSONException e){;}
			}

			if(cfg.has("time"))
			{
				try
                {
	                calendar.setTime(cfg.getLong("time"));
                }
                catch (JSONException e){;}
			}

			if(cfg.has("hardinessZones"))
			{
				try
				{
					hardinessZones=new HardinessZone[cfg.getJSONArray("hardinessZones").length()];
					
					for(int i=0;i<hardinessZones.length;i++)
					{
						hardinessZones[i]=new HardinessZone(cfg.getJSONArray("hardinessZones").getJSONObject(i));
					}
				}
		        catch (JSONException e){;}
			}
			
			if(cfg.has("tempMapGradient"))
			{
				try
                {
					tempMapGradient=cfg.getInt("tempMapGradient");
                }
                catch (JSONException e){;}
			}
			
			if(cfg.has("seasonalPlaceableItemStatePrefixes"))
			{
				try
                {
					seasonalPlaceableItemStatePrefixes=new String[cfg.getJSONArray("seasonalPlaceableItemStatePrefixes").length()];
					
					for(int i=0;i<seasonalPlaceableItemStatePrefixes.length;i++)
					{
						seasonalPlaceableItemStatePrefixes[i]=cfg.getJSONArray("seasonalPlaceableItemStatePrefixes").getString(i);
					}
                }
                catch (JSONException e){;}
			}
			
			if(cfg.has("seasonalPlaceableItemUpdateInterval"))
			{
				try
                {
					seasonalPlaceableItemUpdateInterval=cfg.getLong("seasonalPlaceableItemUpdateInterval");
                }
                catch (JSONException e){;}
			}
		}
		
		if(!WorldGenerator.isNoState())
		{
			secondLength=State.instance().getWorld().getWorldConfigProperty("environment.second.length",secondLength);
			
			temperatureMap=new double[tempMapGradient];
			temperatureMap[temperatureMap.length-1]=WorldGenerator.getRandom().nextDouble();
		}
		
		if(seasonalPlaceableItemStatePrefixes == null)
		{
			seasonalPlaceableItemStatePrefixes=new String[]{
			   "",
			   "fall-",
			   "winter-",
			   "spring-"
			};
		}
	}

	@Override
    public BaseClimateInformation getClimateInformation()
    {
	    return(null);
    }

	@Override
    public BaseWeatherForecast getCurrentWorldwideWeatherForecast()
    {
	    return(null);
    }

	@Override
    public BaseWeather getWeatherForZone(HardinessZone zone)
    {
	    return(zone.getCurrentWeather());
    }

	@Override
    public BaseWeather getWeatherForLocation(Location location)
    {
		Instance i  =State.instance().getWorld().getInstance(location.getInstanceId());
		Biome    b  =i.getTerrain().getBiome(i.getTerrain().getBiomeAt(location.getLocation().getX(),location.getLocation().getY()));
		
		return(
			getWeatherForBiome(
				b
			)
		);
    }

	@Override
    public BaseWeather getWeatherForBiome(Biome biome)
    {
		if(biome == null)
		{
			return(null);
		}
		
		return(
			getWeatherForZone(
				getHardinessZone(
					biome.getHardinessZone()
				)
			)
		);
    }

	@Override
    public double getTemperatureForZone(HardinessZone zone)
    {
		TemperatureGradient tg =zone.getSeasonalTemp(calendar.getSeason());
		double              cmt=getCurrentMeanTemperature();
		double              sp =calendar.getSolarPosition();
		
		//TODO: NEED TO ACCOUNT FOR WEATHER VARIABILITY

	    return(
	    	(
	    		(tg.getMaxTemp()-tg.getMinTemp())*
	    		(cmt            *sp             )
	    	)+
	    	tg.getMinTemp()
	    );
    }

	@Override
    public double getTemperatureForLocation(Location location)
    {
		Instance i  =State.instance().getWorld().getInstance(location.getInstanceId());
		Biome    b  =i.getTerrain().getBiome(i.getTerrain().getBiomeAt(location.getLocation().getX(),location.getLocation().getY()));
		
		return(
			getTemperatureForBiome(
				b
			)
		);
    }

	@Override
    public double getTemperatureForBiome(Biome biome)
    {
		if(biome == null)
		{
			return(20);
		}
		
		return(
			getTemperatureForZone(
				getHardinessZone(
					biome.getHardinessZone()
				)
			)
		);
    }
	
	private double getCurrentMeanTemperature()
	{
		double sp=calendar.getSeasonProgress();
		int    si=(int) Math.floor(sp*(tempMapGradient-1));
		double ip=(sp*(tempMapGradient-1))-si;

		return(
			temperatureMap[si]+((temperatureMap[si+1]-temperatureMap[si])*ip)
		);
	}

	@Override
    public HardinessZone getHardinessZone(int zoneId)
    {
	    for(int i=0;i<hardinessZones.length;i++)
	    {
	    	if(hardinessZones[i].getId() == zoneId)
	    	{
	    		return(hardinessZones[i]);
	    	}
	    }
	    
	    return(hardinessZones[0]);
    }

	@Override
    public String getItemStatePrefixForSeason(int season)
    {
		if(season > getCalendar().getMaximumAllowableSeason())
		{
			season=0;
		}
		else if(season < 0)
		{
			season=getCalendar().getMaximumAllowableSeason();
		}
		
		return(seasonalPlaceableItemStatePrefixes[season]);
    }
	
	@Override
	public String getNamespace()
	{
		return("net.lugdunon.world.defaults.environment");
	}

	@Override
    protected String[] getDefaultConfigs()
    {
	    return(DEFAULT_CONFIGS);
    }

	@Override
    public JSONObject dumpState()
    {
		try
		{
		    JSONObject o=new JSONObject();
		    JSONArray  a=new JSONArray ();
		    JSONArray  b=new JSONArray ();
		    
		    o.put("secondLength",                       secondLength      );
		    o.put("time",                               calendar.getTime());
		    o.put("tempMapGradient",                    tempMapGradient   );
		    o.put("seasonalPlaceableItemUpdateInterval",tempMapGradient   );
		    o.put("seasonalPlaceableItemStatePrefixes", b                 );
		    o.put("hardinessZones",                     a                 );
		    
			for(int i=0;i<hardinessZones.length;i++)
			{
				a.put(hardinessZones[i].toJSONObject());
			}
		    
			for(int i=0;i<seasonalPlaceableItemStatePrefixes.length;i++)
			{
				b.put(seasonalPlaceableItemStatePrefixes[i]);
			}

		    return(o);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return(null);
    }

	@Override
    public void saveState()
    {
		try
		{
		    saveConfigObject("env",dumpState());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }

	@Override
    public void update(long delta)
    {
	    accumulatedDelta+=delta;
	    
	    if(accumulatedDelta >= secondLength)
	    {
	    	getCalendar().setSeconds((int) (getCalendar().getSeconds()+accumulatedDelta/secondLength));
	    	accumulatedDelta=0;
	    	
	    	//GET TEMPERATURE GRADIENT FOR THE SEASON
	    	if(getCalendar().getSeason() != currentSeason)
	    	{
	    		currentSeason    =getCalendar().getSeason();
	    		temperatureMap[0]=temperatureMap[temperatureMap.length-1];
	    		
	    		for(int i=1;i<tempMapGradient;i++)
	    		{
	    			temperatureMap[i]=WorldGenerator.getRandom().nextDouble();
	    		}
	    	}
	    	
	    	//WEATHER
	    	for(HardinessZone hz:hardinessZones)
	    	{
	    		hz.update(this,delta);
	    	}
	    }
    }
	
	public BaseCalendar getCalendar()
	{
		return(this.calendar);
	}
	
	public BaseCalendar newCalendar(long time)
	{
		return(new SimpleGaulishCalendar(time));
	}
	
	////

	@Override
    public long getPlaceableItemUpdateThreshold()
    {
	    return(seasonalPlaceableItemUpdateInterval);
    }

	@Override
    public String getPlaceableItemStatePrefix(PlaceableItemInstance pii)
    {
		if(pii.getItemDef().isSeasonal())
		{
			try
			{
				double sp=getCalendar().getSeasonProgress();
				int    ns=getCalendar().getSeason        ();
				
				if(sp <= 0.1 || sp >= 0.9)
				{
					double sb=pii.getUserDefinedData().getDouble("seasonalBias");
					
					//TRIGGER LATE
					if(sb < 0.5)
					{
						if((sp <= 0.9) || (sp < ((sb-0.5)*0.1)))
						{
							return(getItemStatePrefixForSeason(ns  ));
						}
					}
					//TRIGGER EARLY
					else
					{
						if((sp >= 0.9) && ((sp-0.9) > (sb*0.1)))
						{
							return(getItemStatePrefixForSeason(ns+1));
						}
					}
				}
				else
				{
					return(getItemStatePrefixForSeason(ns  ));
				}
			}
			catch(JSONException e)
			{
				//
			}
		}
		
		return(null);
    }
	
	@Override
	public void totalWeatherUpdate(PlayerCharacter pc)
	{
        try
        {
			CommandProperties props=new CommandProperties();
			
			props.setInt            ("mode",     CommandableWeather.COMMAND_MODE_TOTAL_UPDATE);
			props.setPlayerCharacter("character",pc                                          );
			
            State.instance().getGame().addIncomingRequest(
            	"CORE.COMMAND.ENVIRONMENT.MANAGE.WEATHER",
            	props
            );
        }
        catch (CommandNotSupportedException e)
        {
            e.printStackTrace();
        }
        catch (IsNotServerInvokedCommand e)
        {
            e.printStackTrace();
        }
	}

	@Override
    public void registerEnvironmentListener(IEnvironmentListener el)
    {
		if(!environmentListeners.containsKey(el.getType()))
		{
			environmentListeners.put(el.getType(), new HashMap<String,IEnvironmentListener>());
		}
		
		environmentListeners.get(el.getType()).put(el.getId(),el);
    }

	@Override
    public void unregisterEnvironmentListener(String id)
    {		
		for(String type:environmentListeners.keySet())
		{
			environmentListeners.get(type).remove(id);
		}
    }

	@Override
    public void triggerEnvironmentEvent(String type, Object... args)
    {
		if(environmentListeners.containsKey(type))
		{
			Collection<IEnvironmentListener> els=environmentListeners.get(type).values();
			
		    for(IEnvironmentListener el:els)
		    {
		    	el.environmentEventOccurred(args);
		    }
		}
    }
}