Namespace.declare("net.lugdunon.state.item.action");
Namespace.newClass("net.lugdunon.world.defaults.environment.item.action.spell.AlterWeatherActionHandler","net.lugdunon.state.item.action.IActionHandler");

////

net.lugdunon.world.defaults.environment.item.action.spell.AlterWeatherActionHandler.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.item.action.spell.AlterWeatherActionHandler,"init",[initData]);
	
	return(this);
};

//Ambisagrus = rain
//Tanaris    = thunder
//Leucetios  = fog
//Camulus    = snow
//Aventia    = sun

net.lugdunon.world.defaults.environment.item.action.spell.AlterWeatherActionHandler.prototype.renderToolUseInformation=function(toolDef,durability,type,subtype,spelltype)
{
	return("<div class='ttSubLineItem'>On use, alters the weather in the user's area for a duration of <font color='#0F0'>"+this.getValueFromRandomDefinition(toolDef.value,durability,type,subtype,spelltype)+" minutes</font>.</div>");
};

net.lugdunon.world.defaults.environment.item.action.spell.AlterWeatherActionHandler.prototype.getTriggerMode=function()
{
	return(net.lugdunon.state.item.action.IActionHandler.TRIGGER_MODE_SIMPLE);
};

net.lugdunon.world.defaults.environment.item.action.spell.AlterWeatherActionHandler.prototype.handleUse=function(character,placeable,cursorLoc,actionBarIndex,inputBegan,inputMethod)
{
	game.client.sendCommand(
		game.client.buildCommand(
			"CORE.COMMAND.ENVIRONMENT.MANAGE.WEATHER",
			{
				actionBarIndex:actionBarIndex
			}
		)
	);
	
	return(true);
};