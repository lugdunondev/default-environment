Namespace.declare("net.lugdunon.world.defaults.environment.item.arc.tooltip.information");
Namespace.newClass("net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension","net.lugdunon.state.item.arc.tooltip.information.BaseInformationProviderTooltipExtension");

////

net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension,"init",[initData]);
	return(this);
};


net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.prototype.renderTooltipExtension=function(itemDef,isPlaceableItemInstance,itemInstance)
{	
	var tt=this.callSuper(net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension,"renderTooltipExtension",[itemDef,isPlaceableItemInstance,itemInstance]);
	
	return(tt+this.renderIODesignation());
};

net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.prototype.tooltipDestroyed=function(itemDef,isPlaceableItemInstance,itemInstance)
{
	clearInterval(net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.interval);
	net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.interval=null;
};

net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.prototype.renderInformation=function(itemInstance)
{
	var c=this;
	var a="<div class='ttLineItem' style='color:#FF0'>Current Value</div>"+
		  "<div><div class='ttSubLineItem' style='color:#0F0' id='ARC_INFORMATION_CURRENT_VALUE'>Checking Current Value...</div></div>"+
	      "<div class='ttHalfLineItem'></div>";
	
	clearInterval(net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.interval);
	net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.interval=setInterval(
		function()
		{
			var el=$("#ARC_INFORMATION_CURRENT_VALUE");
			
			if(el.length > 0 && game.isTooltipVisible())
			{
				el.html(game.environment.calendar.toLongString());
				
				game.tooltipUpdated();
			}
		},
		1000
	);
	
	return(a);
};

net.lugdunon.world.defaults.environment.item.arc.tooltip.information.EnvironmentInformationProviderTooltipExtension.prototype.renderIODesignation=function()
{
	var a="<div class='ttLineItem' style='color:#FF0'>Information Input Designation</div>"+
    "<div class='ttSubLineItem'><table class='ioDesignationTable'>";

	a+="<tr>";
	a+="<th>In</th>";
	a+="<th>Designation</th>";
	a+="</tr>";
	
	a+="<tr>";
	a+="<td>0 I<sub>in</sub></td>";
	a+="<td>Mode Toggle 0/1</td>";
	a+="</tr>";
	
	a+="</table></div><div class='ttHalfLineItem'></div>";
	
	a+="<div class='ttLineItem' style='color:#FF0'>Information Output Designation</div>"+
	   "<div class='ttSubLineItem'><table class='ioDesignationTable'>";
	
	a+="<tr>";
	a+="<th>M0 Out</th>";
	a+="<th>Designation</th>";
	a+="</tr>";
	
	a+="<tr>";
	a+="<td>0 I<sub>out</sub></td>";
	a+="<td>Mins as % of hour or 0-59</td>";
	a+="</tr>";
	a+="<tr>";
	a+="<td>1 I<sub>out</sub></td>";
	a+="<td>Hours as % of day or 0-23</td>";
	a+="</tr>";
	a+="<tr>";
	a+="<td>2 I<sub>out</sub></td>";
	a+="<td>Day as % of week or 0-6</td>";
	a+="</tr>";
	a+="<tr>";
	a+="<td>3 I<sub>out</sub></td>";
	a+="<td>Week as % of month or 0-3</td>";
	a+="</tr>";
	a+="<td>4 I<sub>out</sub></td>";
	a+="<td>Month as % of year or 0-11</td>";
	a+="</tr>";
	
	a+="<tr>";
	a+="<th>M1 Out</th>";
	a+="<th>Designation</th>";
	a+="</tr>";
	
	a+="<tr>";
	a+="<td>0 I<sub>out</sub></td>";
	a+="<td>Season as % of year or 0-3</td>";
	a+="</tr>";
	a+="<tr>";
	a+="<td>1 I<sub>out</sub></td>";
	a+="<td>Year</td>";
	a+="</tr>";
	a+="<tr>";
	a+="<td>2 I<sub>out</sub></td>";
	a+="<td>Lunar Phase %</td>";
	a+="</tr>";
	a+="<tr>";
	a+="<td>3 I<sub>out</sub></td>";
	a+="<td>Temperature</td>";
	a+="</tr>";
	a+="<tr>";
	a+="<td>4 I<sub>out</sub></td>";
	a+="<td>Current Weather</td>";
	a+="</tr>";
	
	a+="</table></div><div class='ttHalfLineItem'></div>";
	
	return(a);
};