Namespace.declare("net.lugdunon.world.defaults.environment.item.arc.handler.information");
//Namespace.require("net.lugdunon.ui.recipe.RecipeSelectorDialog");
Namespace.newClass("net.lugdunon.world.defaults.environment.item.arc.handler.information.WeatherInformationProviderArcHandler","net.lugdunon.state.item.arc.handler.information.StringInformationProviderArcHandler");

net.lugdunon.world.defaults.environment.item.arc.handler.information.WeatherInformationProviderArcHandler.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.item.arc.handler.information.WeatherInformationProviderArcHandler,"init",[initData]);

	return(this);
};

net.lugdunon.world.defaults.environment.item.arc.handler.information.WeatherInformationProviderArcHandler.prototype.getInformationConfigurationIcon=function()
{
	return("ACTION_CONFIG_WEATHER");
};

net.lugdunon.world.defaults.environment.item.arc.handler.information.WeatherInformationProviderArcHandler.prototype.handleInformationConfigurationInvocation=function()
{	
	var context={
		label    :this.getInformationConfigurationLabel(),
		icon     :this.getInformationConfigurationIcon (),
		input    :null,
		placeable:this.pii,
		delegate :this
	};
	
	game.client.sendCommand(
		game.client.buildCommand.call(
			game.client,
			"CORE.COMMAND.ARC.INFORMATION.VALUE",
			{
				placeableItemInstanceId:context.placeable.itemInstanceId,
				instanceId             :game.instanceId,
				delegate               :
				{
					arcInformationValueReceived:function(aiv)
					{
						console.log(aiv);
//						new net.lugdunon.ui.item.ItemSelectorDialog().init(
//							{
//								showEmpty :false,
//								showSlider:false,
//								delegate  :context.delegate,
//								item      :
//								{
//									itemId   :((aiv==null||aiv.indexOf(":") == -1)?(null):(aiv.split(":")[1])),
//									stackSize:1
//								}
//							}
//						).show();
					}
				}
			}
		)
	);
};

net.lugdunon.world.defaults.environment.item.arc.handler.information.WeatherInformationProviderArcHandler.prototype.itemSelected=function(item)
{
//	game.client.sendCommand(
//		game.client.buildCommand.call(
//			game.client,
//			"CORE.COMMAND.ARC.INFORMATION.VALUE",
//			{
//				placeableItemInstanceId:this.pii.itemInstanceId,
//				instanceId             :game.instanceId,
//				value                  :((item==null||item.itemId==null)?(null):("ITEM:"+item.itemId)),
//				delegate               :
//				{
//					arcInformationValueReceived:function(aiv)
//					{
//						net.lugdunon.ui.Dialog.ok("Success","<br/>Information value successfully updated.");
//					}
//				}
//			}
//		)
//	);
};