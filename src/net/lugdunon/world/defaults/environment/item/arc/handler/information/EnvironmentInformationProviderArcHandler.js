Namespace.declare("net.lugdunon.world.defaults.environment.item.arc.handler.information");
Namespace.newClass("net.lugdunon.world.defaults.environment.item.arc.handler.information.EnvironmentInformationProviderArcHandler","net.lugdunon.state.item.arc.handler.information.IInformationProviderArcHandler");

net.lugdunon.world.defaults.environment.item.arc.handler.information.EnvironmentInformationProviderArcHandler.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.item.arc.handler.information.EnvironmentInformationProviderArcHandler,"init",[initData]);
	
	return(this);
};