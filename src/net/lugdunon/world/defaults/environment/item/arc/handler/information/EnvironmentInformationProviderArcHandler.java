package net.lugdunon.world.defaults.environment.item.arc.handler.information;

import net.lugdunon.math.Location;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.item.ItemInstance;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.state.item.arc.handler.BaseArcHandler;
import net.lugdunon.state.item.arc.handler.information.IInformationProviderArcHandler;
import net.lugdunon.state.item.arc.handler.information.ex.InformationContractViolatedException;
import net.lugdunon.world.environment.IEnvironment;
import net.lugdunon.world.environment.calendar.BaseCalendar;
import net.lugdunon.world.environment.weather.BaseWeather;

public class EnvironmentInformationProviderArcHandler extends BaseArcHandler implements IInformationProviderArcHandler
{
	public static final int CONDUIT_MINUTES    =0;
	public static final int CONDUIT_HOURS      =1;
	public static final int CONDUIT_DAYS       =2;
	public static final int CONDUIT_WEEKS      =3;
	public static final int CONDUIT_MONTHS     =4;

	public static final int CONDUIT_SEASON     =0;
	public static final int CONDUIT_YEAR       =1;
	public static final int CONDUIT_LUNAR_PHASE=2;
	public static final int CONDUIT_TEMPERATURE=3;
	public static final int CONDUIT_WEATHER    =4;

	IEnvironment environment;
	BaseCalendar calendar   ;
	
	@Override
    public void init(PlaceableItemInstance pii)
    {
	    super.init(pii);
	    
		environment=State.instance().getWorld().getSubsystems().getEnvironment();
		calendar   =environment.getCalendar();
    }

	@Override
	public Object informationRequest(int conduit, int... contract)
	{
		if(getInformationMode() == 0)
		{
			switch(conduit)
			{
				case CONDUIT_MINUTES:
				{
					return(
						processInformationRequest(
							((double) calendar.getMinutes()) / calendar.getMinutesInHour(),
							          calendar.getMinutes(),
							contract
						)
					);
				}
				case CONDUIT_HOURS:
				{
					return(
						processInformationRequest(
							((double) calendar.getHours()) / calendar.getHoursInDay(),
							          calendar.getHours(),
							contract
						)
					);
				}
				case CONDUIT_DAYS:
				{
					return(
						processInformationRequest(
							((double) calendar.getDay()) / calendar.getDaysInWeek(),
							          calendar.getDay(),
							contract
						)
					);
				}
				case CONDUIT_WEEKS:
				{
					return(
						processInformationRequest(
							((double) calendar.getWeek()) / calendar.getWeeksInMonth(),
							          calendar.getWeek(),
							contract
						)
					);
				}
				case CONDUIT_MONTHS:
				{
					return(
						processInformationRequest(
							((double) calendar.getMonth()) / (calendar.getMonthsInSeason()+calendar.getSeasonsInYear()),
							          calendar.getMonth(),
							contract
						)
					);
				}
			}
		}
		else
		{
			switch(conduit)
			{
				case CONDUIT_SEASON:
				{
					return(
						processInformationRequest(
							((double) calendar.getSeason()) / calendar.getSeasonsInYear(),
							          calendar.getSeason(),
							contract
						)
					);
				}
				case CONDUIT_YEAR:
				{
					return(calendar.getYear());
				}
				case CONDUIT_LUNAR_PHASE:
				{
					return(calendar.getLunarPhase());
				}
				case CONDUIT_TEMPERATURE:
				{
					return(
						environment.getTemperatureForLocation(
							new Location(
								pii.getInstanceId(),
								pii.getLocation  ()
							)
						)
					);
				}
				case CONDUIT_WEATHER:
				{
					BaseWeather weather=environment.getWeatherForLocation(new Location(pii.getInstanceId(),pii.getLocation()));

					return((weather == null)?"None (Clear Skies)":weather.getName());
				}
			}
		}

	    return(null);
	}

	@Override
	public boolean conformsToInformationContract(int conduit, int... contract)
	{
		if(getInformationMode() == 0)
		{
			confirmNumericalContract(contract);
		}
		else
		{
			switch(conduit)
			{
				case CONDUIT_SEASON:
				{
					confirmNumericalContract(contract);
				}
				case CONDUIT_YEAR:
				{
					for(int c:contract)
					{
						if(INFORMATION_TYPE_LONG == c)
						{
							return(true);
						}
					}
				}
				case CONDUIT_LUNAR_PHASE:
				{
					for(int c:contract)
					{
						if(INFORMATION_TYPE_DOUBLE == c)
						{
							return(true);
						}
					}
				}
				case CONDUIT_TEMPERATURE:
				{
					confirmFloatingPointContract(contract);
				}
				case CONDUIT_WEATHER:
				{
					for(int c:contract)
					{
						if(INFORMATION_TYPE_STRING == c)
						{
							return(true);
						}
					}
				}
			}
		}
	    
	    return(false);
	}

	private long getInformationMode()
	{
		ItemInstance ii=pii.getInventoryItem(0,Character.CONDUIT_INFORMATION_IN_BLOCK);
		long         m =0;
		
		if(ii != null)
		{
			PlaceableItemInstance io=ii.getActiveSiblingLocation(Character.CONDUIT_INFORMATION_OUT_BLOCK);
			
			if(io != null)
			{
				m=(Long) ((IInformationProviderArcHandler) io.getArcHandler()).informationRequest(
					ii.indexOfSibling(Character.CONDUIT_INFORMATION_OUT_BLOCK),
					IInformationProviderArcHandler.INFORMATION_TYPE_LONG
				);
			}
		}
		
		return(m);
	}
	
	@Override
	public void updateInformationValue(Object value) throws InformationContractViolatedException
	{
		//DO NOTHING
	}
	
	@Override
	public Object getInformationValue()
	{
		return(calendar.toLongString());
	}

	private Object processInformationRequest(double doubleValue, long longValue, int... contract)
    {
		for(int c:contract)
		{
			if(INFORMATION_TYPE_DOUBLE == c)
			{
				return(doubleValue);
			}
			else if(INFORMATION_TYPE_LONG == c)
			{
				return(longValue);
			}
		}
		
		return(null);
    }
	
	private boolean confirmFloatingPointContract(int[] contract)
    {
		for(int c:contract)
		{
			if(INFORMATION_TYPE_DOUBLE == c)
			{
				return(true);
			}
		}
	    
	    return(false);
    }
	
	private boolean confirmNumericalContract(int[] contract)
    {
		for(int c:contract)
		{
			if(
				(INFORMATION_TYPE_DOUBLE == c)
				||
				(INFORMATION_TYPE_LONG   == c)
			)
			{
				return(true);
			}
		}
	    
	    return(false);
    }
}