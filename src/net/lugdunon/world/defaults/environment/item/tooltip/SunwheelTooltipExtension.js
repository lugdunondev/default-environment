Namespace.declare("net.lugdunon.world.defaults.environment.item.tooltip");
Namespace.newClass("net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension","net.lugdunon.state.item.tooltip.ITooltipExtension");

////

net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.CANVAS;
net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.CONTEXT;

net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_SEASONS;
net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_SEASONS_OVERLAY;
net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_DATE_INDICATOR;

net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension,"init",[initData]);
	
	this.w=256;
	this.h=256;
	
	//MAKE CANVAS & CONTEXT
	if(net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.CANVAS == null)
	{
		net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_SEASONS        =assetManager.getImage("ENV_SEASONS"        );
		net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_SEASONS_OVERLAY=assetManager.getImage("ENV_SEASONS_OVERLAY");
		net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_DATE_INDICATOR =assetManager.getImage("ENV_DATE_INDICATOR" );
		
		net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.CANVAS             =game.makeCanvas(this.w,this.h);
		net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.CONTEXT            =net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.CANVAS.getContext("2d");
	}
	
	this.ctxt         =net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.CONTEXT;
	this.seasonWidth  =(2*Math.PI)*(1/game.environment.getCalendar().getSeasonsInYear());
	this.seasonPercent=(2*Math.PI)-this.seasonWidth;
	
	return(this);
};

net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.prototype.drawBackground=function()
{
	this.ctxt.clearRect(0,0,this.w,this.h);
	
	this.ctxt.drawImage(
		net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_SEASONS,
		0,0,
		this.w,
		this.h
	);
};

net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.prototype.drawSeason=function()
{
	this.ctxt.save();
	this.ctxt.beginPath();
	this.ctxt.moveTo(128, 128);
	this.ctxt.arc(
		128,128,
		128,
		this.seasonWidth*(game.environment.getCalendar().getSeason()+1),
		(this.seasonWidth*(game.environment.getCalendar().getSeason()+1))+this.seasonPercent
	);
	this.ctxt.clip();
	
	this.ctxt.drawImage(
		net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_SEASONS_OVERLAY,
		0,0,
		this.w,
		this.h
	);
	
	this.ctxt.restore();
};

net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.prototype.drawDateIndicator=function()
{
	this.ctxt.save();

	this.ctxt.translate(128, 128);
	this.ctxt.rotate(
		//START OF SEASON
		(this.seasonWidth*(game.environment.getCalendar().getSeason()+1))+
		//SEAONAL PROGRESS
		(this.seasonWidth*game.environment.getCalendar().getSeasonProgress())
	);
	this.ctxt.translate(-128, -128);
	
	this.ctxt.drawImage(
		net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.ENV_DATE_INDICATOR,
		0,0,
		this.w,
		this.h
	);
	
	this.ctxt.restore();
};

net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.prototype.renderTooltipExtension=function(itemDef,isPlaceableItemInstance,itemInstance)
{
	if(itemInstance != null && itemInstance.itemInstanceId > -1)
	{
		//UPDATE CONTEXT ACCORDING TO TIME
		this.drawBackground   ();
		this.drawSeason       ();
		this.drawDateIndicator();
		
		//RETURN HTML WITH CANVAS
		return(
			"<div class='ttLineItem' style='height: 256px;'><div class='tooltipSunwheel' style='background-image:url("+
			net.lugdunon.world.defaults.environment.item.tooltip.SunwheelTooltipExtension.CANVAS.toDataURL()+
			");'></div></div>"
		);
	}
	
	return(null);
};